#define _THREAD_SAFE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


/*fixed*/
void * functionMultiThread(void * index){
	int target;
	int *p;
	p=malloc(sizeof(int)); 
	target = *((int *) index);
	target = target * 2;
	*p = target;
	printf("%d\n", *p);
	fflush(stdout);
	pthread_exit((void *) p);
}


int main(void){
	int *p;
	int i,sum;
	pthread_t thread_identifier[10];

	sum=0;
	for(i=0;i<10;i++){
		p=malloc(sizeof(int)); /*alloco p e lo setto a i*/
		if(p!=NULL){
			*p=i;
			pthread_create(&(thread_identifier[i]), NULL, functionMultiThread, (void *)p); /*o passo al thread*/
		}
		sum+=i;
	}
	printf("La somma è %d\n", sum);
	
	sum=0;
	for(i=0;i<10;i++){
		p=NULL; /*tanto poi viene aggiornato in pthread join*/
		pthread_join(thread_identifier[i], (void **) &p); /*mi aspetto il risultato su p*/
		printf("%d\n",*p);
		fflush(stdout);
		sum+=(*p); /*lo converto a intero e sommo*/
	}
	printf("La somma è %d", sum);
	return 0;
}