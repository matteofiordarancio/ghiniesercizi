#!/bin/bash

INC=0
FILENAMELIST=`find ./ -maxdepth 1 -name "bash[[:digit:]]*.sh" -print | sort`

for FILENAME in ${FILENAMELIST} ; do
	reset
	if (( "${INC}"=="4" || "${INC}"=="6" )); then
		${FILENAME} "primo secondo" terzo
	else
		${FILENAME}
	fi
	read -n1 -r -p "Press any key to continue..." key
	((INC=${INC}+1))
done