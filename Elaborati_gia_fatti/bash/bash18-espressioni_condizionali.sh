#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}18 ESPRESSIONI CONDIZIONALI${N}

${B}[[ -d${N} ${U}file${N} ${B}]]${N} 			True if file exists and is a directory.
${B}[[ -e${N} ${U}file${N} ${B}]]${N} 			True if file exists.
${B}[[ -f${N} ${U}file${N} ${B}]]${N} 			True if file exists and is a regular file.
${B}[[ -h${N} ${U}file${N} ${B}]]${N} 			True if file exists and is a symbolic link.
${B}[[ -r${N} ${U}file${N} ${B}]]${N} 			True if file exists and is readable.
${B}[[ -s${N} ${U}file${N} ${B}]]${N} 			True if file exists and has a size greater than zero.
${B}[[ -t${N} ${U}fd${N} ${B}]]${N} 			True if file descriptor fd is open and refers to  a  terminal. 
${B}[[ -w${N} ${U}file${N} ${B}]]${N} 			True if file exists and is writable.
${B}[[ -x${N} ${U}file${N} ${B}]]${N} 			True if file exists and is executable.
${B}[[ -O${N} ${U}file${N} ${B}]]${N} 			True if file exists and is owned by the effective user id.
${B}[[ -G${N} ${U}file${N} ${B}]]${N} 			True if file exists and is owned by the effective group id.
${B}[[ -L${N} ${U}file${N} ${B}]]${N} 			True if file exists and is a symbolic link.

${B}[[ -z${N} ${U}string${N} ${B}]]${N} 		True if the length of string is zero.
${B}[[ -n${N} ${U}string${N} ${B}]]${N} 		True if the length of string is non-zero.
${B}[[ ${N}${U}string1${N} ${B}==${N} ${U}string2${N} ${B}]]${N} 	True if the strings are equal 
${B}[[ ${N}${U}string1${N} ${B}!=${N} ${U}string2${N} ${B}]]${N} 	True if the strings are not equal. 
${B}[[ ${N}${U}string1${N} ${B}<${N} ${U}string2${N} ${B}]]${N} 	True if string1 sorts before string2 lexicographically.
${B}[[ ${N}${U}string1${N} ${B}>${N} ${U}string2${N} ${B}]]${N} 	True if string1 sorts after string2 lexicographically.

${B}[[ ${N}${U}arg1${N} ${B}OP${N} ${U}arg2${N} ${B}]]${N} 		OP is one of -eq, -ne, -lt, -le, -gt, -ge.
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
INPUT=input.txt
OUTPUT=output.txt

if ls ${INPUT} ; then
	echo "Il file ${INPUT} esiste" ;
else
	echo "Il file ${INPUT} non esiste" ;
fi

if [[ -e input.txt && -e output.txt ]] ; then
	echo "I file ${INPUT}, ${OUTPUT} esistono" ;
else
	echo "I file ${INPUT}, ${OUTPUT} non esistono" ;
fi
# FINE ESEMPIO
echo
