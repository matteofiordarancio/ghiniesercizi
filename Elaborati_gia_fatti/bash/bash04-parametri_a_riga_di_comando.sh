#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}04 PARAMETRI A RIGA DI COMANDO${N}

${B}\$#${N} 	Il numero di argomenti passati allo script
${B}\$0${N} 	Il nome del processo in esecuzione 
${B}\$1${N} 	Primo argomento
${B}\$2${N}	Secondo argomento
${B}\$*${N} 	Tutti gli argomenti passati a riga di comando concatenati e separati da spazi
${B}\$@${N} 	Simile a \$*, ma quando quotato permette di non spezzare gli argomenti che contengono degli spazi
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
echo "$# argomenti passati alla shell" 
echo "Il nome del processo in esecuzione è \"$0\"" 
echo "Gli argomenti passati sono \"$*\""
NUM=1
for ITEM in "$@" ; do 
	echo "L'argomento ${NUM} è \"${ITEM}\"" ;
	((NUM=${NUM}+1))
done
# FINE ESEMPIO
echo
