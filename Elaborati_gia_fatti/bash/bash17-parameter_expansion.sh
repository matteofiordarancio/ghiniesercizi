#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}17 PARAMETER EXPANSION${N}

${B}\${${N}${U}VAR${N}${B}%%${N}${U}pattern${N}${B}}${N} 	Rimuove il più lungo suffisso che fa match con stringa originale
${B}\${${N}${U}VAR${N}${B}%${N}${U}pattern${N}${B}}${N} 	 	Rimuove il più corto suffisso che fa match con stringa originale
${B}\${${N}${U}VAR${N}${B}##${N}${U}pattern${N}${B}}${N} 	Rimuove il più lungo prefisso che fa match con stringa originale
${B}\${${N}${U}VAR${N}${B}#${N}${U}pattern${N}${B}}${N} 	 	Rimuove il più corto prefisso che fa match con stringa originale
${B}\${${N}${B}#${N}${U}VAR${N}${B}}${N} 		Restituisce la stringa che esprime la lunghezza del contenuto di VAR${N}

${B}\${${N}${U}VAR${N}${B}/${N}${U}pattern${N}${B}/${N}${U}string${N}${B}}${N} 	Cerca nel contenuto di VAR la prima sottostringa più lunga che fa match con il pattern specificato e lo sostituisce con string
${B}\${${N}${U}VAR${N}${B}:${N}${U}offset${N}${B}}${N} 		Sottostringa che parte dal offset-esimo carattere del contenuto di VAR 
${B}\${${N}${U}VAR${N}${B}:${N}${U}offset${N}${B}:${N}${U}length${N}${B}}${N} 	Sottostringa lunga length che parte dal offset-esimo carattere del contenuto di VAR  

${B}\${${N}${U}VAR${N}${B}//${N}${U}pattern${N}${B}/${N}${U}string${N}${B}}${N} 	Simile a \$VAR/pattern/string} ma il pattern viene sostituito con tutte le sottostringhe che fanno match
${B}\${${N}${U}VAR${N}${B}/#${N}${U}pattern${N}${B}/${N}${U}string${N}${B}}${N} 	Simile a \$VAR/pattern/string} ma il pattern viene sostituito solo se si trova all'inizio della variabile
${B}\${${N}${U}VAR${N}${B}/%${N}${U}pattern${N}${B}/${N}${U}string${N}${B}}${N} 	Simile a \$VAR/pattern/string} ma il pattern viene sostituito solo se si trova alla fine della variabile

${B}\${!${N}${U}VarNamePrefix${N}${B}*}${N} 	Restituisce un elenco con tutti i nomi delle variabili il cui nome inizia con il prefisso specificato VarNamePrefix 
"
