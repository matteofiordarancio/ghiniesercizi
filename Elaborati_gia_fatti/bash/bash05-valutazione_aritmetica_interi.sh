#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}05 VALUTAZIONE ARITMETICA INTERI${N}

L'operatore ${B}((${N}${U}...${N}${B}))${N} racchiude una intera riga di comando
L'operatore ${B}\$((${N}${U}...${N}${B}))${N} racchiude anche parte di una riga di comando
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
VAR=1
((VAR=${VAR}+3))
echo  ${VAR}

VAR=1
VAR=foo$((${VAR}+5))
echo  ${VAR}
# FINE ESEMPIO
echo
