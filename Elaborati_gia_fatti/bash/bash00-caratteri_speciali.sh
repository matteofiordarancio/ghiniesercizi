#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}00 CARATTERI SPECIALI${N}

${R} AltGr ${N} + ${R} ' ${N} = ${B}\`${N}

${R} AltGr ${N} + ${R} ì ${N} = ${B}~${N}

${R} AltGr ${N} + ${R} è ${N} = ${B}[${N}

${R} AltGr ${N} + ${R} + ${N} = ${B}]${N}

${R} AltGr ${N} + ${R} Shift ${N} + ${R} è ${N} = ${B}{${N}

${R} AltGr ${N} + ${R} Shift ${N} + ${R} + ${N} = ${B}}${N}
"
