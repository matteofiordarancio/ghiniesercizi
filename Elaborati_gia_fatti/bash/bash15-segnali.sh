#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}15 SEGNALI${N}

${B}kill${N} <pid> 		Segnale di terminazione del processo
${B}kill${N} -${U}SIGNAL${N} <pid> 	Altri tipi di segnali al processo
${B}kill${N} -l 		Visualizza l'elenco dei segnali gestiti
${B}killall${N} -r ${U}name${N} 	Termina tutti i processi che hanno lo stesso nome
"
