#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}06 RIFERIMENTI INDIRETTI A VARIABILI${N}

${B}\${!${N}${U}VAR${N}${B}}${N} 	Riferimento indiretto alla variabile
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
echo "$# argomenti passati alla shell" 
echo "Il nome del processo in esecuzione è \"$0\"" 
echo "Gli argomenti passati sono \"$*\""
INC=1
while (( "${INC}"<="$#" )); do
	echo "L'argomento numero ${INC} è \"${!INC}\""
	((INC=${INC}+1))
done
# FINE ESEMPIO
echo
