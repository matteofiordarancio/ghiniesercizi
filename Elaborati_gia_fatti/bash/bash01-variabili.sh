#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}01 VARIABILI${N}
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
# creazione e assegnazione di una variabile locale (non disponibile per i sottoprocessi)
LOCAL=123
echo ${LOCAL}

# creazione e assegnazione di una variabile di ambiente (disponibile per i sottoprocessi)
export ENVIRONEMENT=456
echo ${ENVIRONEMENT}

# esecuzione di uno script creando una subshell
./subshell.sh
echo ${ENVIRONEMENT}

# esecuzione di uno script senza creare una subshell
source ./subshell.sh
echo ${ENVIRONEMENT}

# esecuzione di uno script creando una subshell e passando variabili (che diventeranno di ambiente)
VAR=${LOCAL} ./subshell2.sh

# eliminazione di una variabile
unset LOCAL
# FINE ESEMPIO
echo
