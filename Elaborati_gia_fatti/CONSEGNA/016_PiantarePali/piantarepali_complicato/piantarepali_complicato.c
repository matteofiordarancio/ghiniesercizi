/* piantarepali */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define NUMPOLES 2
#define NUMHAMMER 2

#define TODO 0
#define DONE 1

#define NOTGIVEN 0
#define GIVEN 1

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int numHammerDoing;
	int numHammerDone;
	int hammerStatus[NUMHAMMER];
	int commandHammerStart;

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condHammerStart;
	pthread_cond_t condHammerFinish;
} shmo_t;

void *poleHolder(shmo_t *shmo, int index)
{
	int i;
	int loopCounter = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread poleHolder %lu", (unsigned long int) index);

	while (1) {
		printf("Pali presi\n");
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->numHammerDoing = 0;
		shmo->numHammerDone = 0;
		for (i = 0; i < NUMHAMMER; ++i) {
			(shmo->hammerStatus)[i] = TODO;
		}

		printf("Martellare!\n");
		fflush(stdout);
		shmo->commandHammerStart = GIVEN;
		DBGpthread_cond_broadcast(&(shmo->condHammerStart), label);

		DBGpthread_cond_wait(&(shmo->condHammerFinish), &(shmo->mutex), label);

		loopCounter++;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		if (loopCounter % 5 == 0) {
			printf("Reggipali%d in pausa\n", index);
			fflush(stdout);
			sleep(3);
		}
	}
}

void *hammer(shmo_t *shmo, int index)
{
	int loopCounter = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread hammer %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->commandHammerStart == NOTGIVEN || shmo->hammerStatus[index] == DONE) {
			DBGpthread_cond_wait(&(shmo->condHammerStart), &(shmo->mutex), label);
		}

		printf("Martello%d inizia\n", index);
		fflush(stdout);
		shmo->numHammerDoing++;
		if (shmo->numHammerDoing == NUMPOLES) {
			shmo->commandHammerStart = NOTGIVEN;
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		(shmo->hammerStatus)[index] = DONE;
		shmo->numHammerDone++;
		printf("Martello%d finisce\n", index);
		fflush(stdout);
		if (shmo->numHammerDone == NUMPOLES) {
			DBGpthread_cond_signal(&(shmo->condHammerFinish), label);
		}

		loopCounter++;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		if (loopCounter % 4 == 0) {
			printf("Martello%d in pausa\n", index);
			fflush(stdout);
			if (index == 0) {
				sleep(5);
			}
			else if (index == 1) {
				sleep(7);
			}
		}
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->numHammerDoing = 0;
	shmo->numHammerDone = 0;
	for (i = 0; i < NUMHAMMER; ++i) {
		(shmo->hammerStatus)[i] = TODO;
	}
	shmo->commandHammerStart = NOTGIVEN;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condHammerStart), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condHammerFinish), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < NUMHAMMER; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			hammer(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	i = 0;
	poleHolder(shmo, i);

	return(0);
}
