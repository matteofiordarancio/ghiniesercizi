/* cani_bicilette */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMDOG 10

#define LEFTANKLE 0
#define RIGHTANKLE 1

/* dati da proteggere */
int flag = 0;
int lastNumberEmitted = 0;
int turn = 0;
int numBiteAnkleLeft = 0;
int numBiteAnkleRight = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condBite;
pthread_cond_t condKick;
pthread_cond_t condTurn;


void *bicycle(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread bicycle %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (numBiteAnkleLeft == 0 && numBiteAnkleRight == 0) {
			DBGpthread_cond_wait(&condBite, &mutex, label);
		}

		DBGpthread_cond_signal(&condKick, label);
		printf("il ciclista calcia\n");

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		DBGnanosleep(500000000, label);

	}
}

void *dog(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int myNumber = 0;
	int bittenAnkle = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread dog %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		myNumber = lastNumberEmitted;
		/*lastNumberEmitted++;*/
		lastNumberEmitted = (lastNumberEmitted + 1) % NUMDOG;

		if (flag == 0 && myNumber > 3) {
			flag = 1;
		}
		while (flag == 1 && myNumber != turn) {
			DBGpthread_cond_wait(&condTurn, &mutex, label);
		}

		if (numBiteAnkleLeft < 2)
		{
			numBiteAnkleLeft++;
			bittenAnkle = LEFTANKLE;
		}
		else if (numBiteAnkleRight < 2)
		{
			numBiteAnkleRight++;
			bittenAnkle = RIGHTANKLE;
		}

		DBGpthread_cond_signal(&condBite, label);

		printf("il cane %"PRIiPTR" col numero %d ha morso la caviglia %s \n", index, myNumber, (bittenAnkle == LEFTANKLE ? "sinistra" : "destra"));

		DBGpthread_cond_wait(&condKick, &mutex, label);

		if (bittenAnkle == LEFTANKLE)
		{
			numBiteAnkleLeft--;
		}
		else {
			numBiteAnkleRight--;
		}

		/*turn++;*/
		turn = (turn + 1) % NUMDOG;

		DBGpthread_cond_broadcast(&condTurn, label);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condBite, NULL, "cond");
	DBGpthread_cond_init(&condKick, NULL, "cond");
	DBGpthread_cond_init(&condTurn, NULL, "cond");

	/* imposta le condizioni iniziali */
	flag = 0;
	lastNumberEmitted = 0;
	turn = 3;
	numBiteAnkleLeft = 0;
	numBiteAnkleRight = 0;


	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, bicycle, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	for (i = 0; i < NUMDOG; i++) {
		rc = pthread_create(&tid, NULL, dog, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
