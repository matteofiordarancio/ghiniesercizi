/* N Produttori e N Consumatori che si scambiano prodotti mediante un unico Buffer */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif


#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMCAVERNICOLI_LATO_A 2
#define NUMCAVERNICOLI_LATO_B 1
#define NUMPOSTI 2

/* dati da proteggere */
int NumPostiLiberi = NUMPOSTI;
int turno_lato_A = 0;
int turno_lato_B = 0;


/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condAttraversato;
pthread_cond_t condScesi;
pthread_cond_t condSaliti;
pthread_cond_t condAttendonoLatoA;
pthread_cond_t condAttendonoLatoB;

void *dinosauro(void *arg)
{
	intptr_t index = (intptr_t)arg;
	char lato='A';
	
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread dinosauro %lu"PRIiPTR, (unsigned long int) index);

	DBGpthread_mutex_lock(&mutex,label); 
	
	while(1) {
		
		if(lato == 'A') {
		    turno_lato_A = 1;
		    DBGpthread_cond_broadcast(&condAttendonoLatoA,label); 
		} else {
		    turno_lato_B = 1;
		    DBGpthread_cond_broadcast(&condAttendonoLatoB,label); 
		}

		printf("dinosauro che è sul lato %c ha avvisato di salire \n", lato);
		fflush(stdout);

		NumPostiLiberi = NUMPOSTI;

		DBGpthread_cond_wait(&condSaliti,&mutex,label);
		
		turno_lato_A = 0;
		turno_lato_B = 0;
		
		printf("dinosauro parte dal lato %c \n", lato);
		fflush(stdout);

		DBGpthread_mutex_unlock(&mutex,label); 
		sleep(2);

		if(lato=='A') {
			lato='B';
		} else {
			lato='A';
		}
		
		DBGpthread_mutex_lock(&mutex,label);
		printf("dinosauro avverte i cavernicoli di scendere sul lato %c \n", lato);
		fflush(stdout);
		
		DBGpthread_cond_broadcast(&condAttraversato,label); 
		printf("dinosauro ha fatto scendere i cavernicoli sul lato %c \n", lato);
		fflush(stdout);

		/* ora deve attendere che siano scesi */
		DBGpthread_cond_wait(&condScesi,&mutex,label);
	}
	pthread_exit(NULL); 
}

void *cavernicolo(void *arg)
{
	intptr_t index = (intptr_t)arg;
	char lato;
	
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread cavernicolo %lu"PRIiPTR, (unsigned long int) index);

	if(index < NUMCAVERNICOLI_LATO_A) {
		lato='A';
	}else{
		lato='B';
	}

	while(1) {
		
		DBGpthread_mutex_lock(&mutex,label); 
		if(lato == 'A') {
		    while(	turno_lato_A == 0  ||  NumPostiLiberi == 0  )
				DBGpthread_cond_wait(&condAttendonoLatoA,&mutex,label);
		} else {
		    while(	turno_lato_B == 0  ||  NumPostiLiberi == 0  )
				DBGpthread_cond_wait(&condAttendonoLatoB,&mutex,label);
		}

		NumPostiLiberi--;

		printf("cavericolo %"PRIiPTR" salito dal lato %c \n", index, lato);
		fflush(stdout);

		if(NumPostiLiberi == 0) {
			DBGpthread_cond_signal(&condSaliti,label);
		}

		DBGpthread_cond_wait(&condAttraversato,&mutex,label);
		
		NumPostiLiberi++;
		if(NumPostiLiberi == NUMPOSTI)
			DBGpthread_cond_signal(&condScesi,label); 

		
		if(lato=='A') {
			lato='B';
		} else {
			lato='A';
		}

		printf("cavericolo %"PRIiPTR" sceso nel lato %c \n", index, lato);
		fflush(stdout);

		DBGpthread_mutex_unlock(&mutex, label); 

		sleep(4);
	}
	pthread_exit(NULL); 
}

int main()
{
	pthread_t tid;
	int rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condAttraversato, NULL, "cond_init");
	DBGpthread_cond_init(&condScesi, NULL, "cond_init");
	DBGpthread_cond_init(&condSaliti, NULL, "cond_init");
	DBGpthread_cond_init(&condAttendonoLatoA, NULL, "cond_init");
	DBGpthread_cond_init(&condAttendonoLatoB, NULL, "cond_init");

	/* imposta le condizioni iniziali */
	turno_lato_A = 1;
	turno_lato_B = 0;

	/* crea i thread */
	for(i=0;i<NUMCAVERNICOLI_LATO_A+NUMCAVERNICOLI_LATO_B;i++) {
		rc=pthread_create(&tid,NULL,cavernicolo,(void*)i); 
		if(rc) PrintERROR_andExit(errno,"pthread_create failed");
	}
	i=0;
	rc=pthread_create(&tid,NULL,dinosauro,(void*)i); 
	if(rc) PrintERROR_andExit(errno,"pthread_create failed");

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
