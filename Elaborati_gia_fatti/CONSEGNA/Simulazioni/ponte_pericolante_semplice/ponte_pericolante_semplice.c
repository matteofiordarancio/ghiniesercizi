/* Algoritmo del Fornaio */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMAUTO_ORARIO 4
#define NUMAUTO_ANTIORARIO 4
#define CODICE_ORARIO 0
#define CODICE_ANTIORARIO 1

/* dati da proteggere */
int numeriDisplay[2];
int numAutoInAttesa[2];
int ultimiBigliettiEmessi[2];
int PonteOccupato=0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_mutex_t mutexDistributori[2];
pthread_cond_t condPonteLibero;


void *automobile(void *arg)
{
	intptr_t index = (intptr_t)arg;
	char label[100];
	int mioBiglietto;
	int codiceSenso;
	char senso; /* O orario  A antiorario */

	index=(intptr_t)arg;
	if(index<NUMAUTO_ORARIO){
		senso='O';
		codiceSenso=CODICE_ORARIO;
	} else {
		senso='A';
		codiceSenso=CODICE_ANTIORARIO;
	}

	sprintf(label,"%"PRIiPTR"%c", index, senso);

	while(1) {
		DBGpthread_mutex_lock(&mutexDistributori[codiceSenso],label); 
		mioBiglietto = numeriDisplay[codiceSenso];
		numeriDisplay[codiceSenso]++;
		DBGpthread_mutex_unlock(&mutexDistributori[codiceSenso],label); 

		printf("l'Auto %s ha preso ultimiBigliettiEmessi %i \n", label, mioBiglietto);
		fflush(stdout);

		DBGpthread_mutex_lock(&mutex,label); 
		numAutoInAttesa[codiceSenso]++;
		while(PonteOccupato 
				|| (mioBiglietto > ultimiBigliettiEmessi[codiceSenso] )/* il mio biglietto è successivo all'ultimo emesso nel mio senso */
				|| (numAutoInAttesa[codiceSenso] < numAutoInAttesa[!codiceSenso]) /* limita starvation */
				|| (numAutoInAttesa[codiceSenso] == numAutoInAttesa[!codiceSenso] && codiceSenso == CODICE_ANTIORARIO) /* precedenza orari */
				) {
			DBGpthread_cond_wait(&condPonteLibero,&mutex,label);
		}
		numAutoInAttesa[codiceSenso]--;
		PonteOccupato=1;
		ultimiBigliettiEmessi[codiceSenso]++;
		DBGpthread_mutex_unlock(&mutex,label); 

		sleep(1);
		printf("l'Auto %s con il ultimiBigliettiEmessi %i ha attraversato il ponte \n", label, mioBiglietto);
		fflush(stdout);

		DBGpthread_mutex_lock(&mutex,label); 
		PonteOccupato=0;
		DBGpthread_cond_broadcast(&condPonteLibero,label);
		DBGpthread_mutex_unlock(&mutex,label); 

		sleep(5);
		/*printf("L'Auto %s ha fatto il giro \n", label );
		fflush(stdout);*/
	}	

	pthread_exit(NULL); 
}

int main()
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");
	/*DBGpthread_mutex_init(&mutexDistributori, NULL, "mutex_init");*/
	for(i=0; i<2; i++ ) {
		DBGpthread_mutex_init(&mutexDistributori[i], NULL, "mutex_init");
	}

	/* inizializzazione cond */
	DBGpthread_cond_init(&condPonteLibero, NULL, "cond_init");

	/* imposta le condizioni iniziali */
	for(i=0; i<2; i++ ) {
		numAutoInAttesa[i]=0;
		numeriDisplay[i]=0;
		ultimiBigliettiEmessi[i]=0;
	}

	/* crea i thread */
	for (i = 0; i < NUMAUTO_ORARIO + NUMAUTO_ANTIORARIO; i++) {
		rc = pthread_create(&tid, NULL, automobile, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
