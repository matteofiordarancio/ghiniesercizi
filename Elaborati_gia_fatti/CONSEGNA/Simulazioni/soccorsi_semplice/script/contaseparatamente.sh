#!/bin/bash

LIMIT=0
if (( "$#">"9" )); then
	LIMIT=9
else
	LIMIT=$#
fi

INC=1
EVENNUMROW=0
ODDNUMROW=0
while (( "${INC}"<="${LIMIT}" )); do
	if [[ -e ${!INC} ]]; then
		DELTA=$(wc -l ${!INC})
		DELTA=${DELTA%% *}
		if (( ${INC}%2 == 0 )); then
			((EVENNUMROW=${EVENNUMROW}+${DELTA}))
		else
			((ODDNUMROW=${ODDNUMROW}+${DELTA}))
		fi
	fi
	((INC=${INC}+1))
done

echo "${ODDNUMROW}" 1>&2
echo "${EVENNUMROW}"
