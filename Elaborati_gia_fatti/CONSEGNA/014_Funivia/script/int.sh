#!/bin/bash

# per ogni nome di file nella directory /usr/include/linux/ con estensione .h
#for FILENAME in $(find /usr/include/linux/ -name "*.h"); do
for FILENAME in $(find ./ -name "int.txt"); do
	# se all'interno del file int è presente almeno una volta
	if (( "$(grep int ${FILENAME} | wc -l)">="1" )); then
		# per ogni file selezionato
		exec {FD}<${FILENAME}
		while read -u ${FD} ROW; do
			# per ogni riga
			STRING=$(echo ${ROW} | grep int)
			if [[ -n ${STRING:0:3} ]]; then
				echo "${STRING:0:3}"
			fi
		done
		exec {FD}>&-
	fi
done

exit 0
