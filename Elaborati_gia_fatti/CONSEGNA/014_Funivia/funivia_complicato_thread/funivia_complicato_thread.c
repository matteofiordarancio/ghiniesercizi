/* funivia */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define CABLEWAY_LEAVING 0
#define CABLEWAY_ARRIVED 1

#define NUM_SOBERSEAT 2
#define NUM_DRUNKENSEAT 1

#define NUM_SOBER 4
#define NUM_DRUNKEN 2

/* dati da proteggere */
int cableWayStatus = 0;
int occupiedSeatBySober = 0;
int occupiedSeatByDrunken = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condCableWayLeaving;
pthread_cond_t condPassengerOnBoard;
pthread_cond_t condCableWayArrived;
pthread_cond_t condPassengerOffBoard;


void *cableWay(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread cableWay %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		cableWayStatus = CABLEWAY_LEAVING;
		DBGpthread_cond_broadcast(&condCableWayLeaving, label);

		if (occupiedSeatBySober < NUM_SOBERSEAT || occupiedSeatByDrunken < NUM_DRUNKENSEAT){
			DBGpthread_cond_wait(&condPassengerOnBoard, &mutex, label);
		} 
		
		printf("I passeggeri a bordo sono %d sobri e %d ubriachi\n", occupiedSeatBySober, occupiedSeatByDrunken);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(1);
		printf("La funivia è in cima al campanile\n");
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		cableWayStatus = CABLEWAY_ARRIVED;
		DBGpthread_cond_broadcast(&condCableWayArrived, label);

		if (occupiedSeatBySober != 0 || occupiedSeatByDrunken != 0){
			DBGpthread_cond_wait(&condPassengerOffBoard, &mutex, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

void *sober(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread sober passenger %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (cableWayStatus != CABLEWAY_LEAVING || occupiedSeatByDrunken != 0 || occupiedSeatBySober == NUM_SOBERSEAT) {
			DBGpthread_cond_wait(&condCableWayLeaving, &mutex, label);
		}

		occupiedSeatBySober++;
		printf("Il passeggero sobrio %"PRIiPTR" è salito a bordo\n", index);
		if (occupiedSeatBySober == NUM_SOBERSEAT) {
			DBGpthread_cond_signal(&condPassengerOnBoard,label);
		}
		
		while (cableWayStatus != CABLEWAY_ARRIVED) {
			DBGpthread_cond_wait(&condCableWayArrived, &mutex, label);
		}

		occupiedSeatBySober--;
		if (occupiedSeatBySober == 0) {
			DBGpthread_cond_signal(&condPassengerOffBoard, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Il passeggero sobrio %"PRIiPTR" grida di gioia\n", index);
	}
}

void *drunken(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread drunken passenger %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (cableWayStatus != CABLEWAY_LEAVING || occupiedSeatBySober != 0 || occupiedSeatByDrunken == NUM_DRUNKENSEAT) {
			DBGpthread_cond_wait(&condCableWayLeaving, &mutex, label);
		}

		occupiedSeatByDrunken++;
		printf("Il passeggero ubriaco %"PRIiPTR" è salito a bordo\n", index);
		if (occupiedSeatByDrunken == NUM_DRUNKENSEAT) {
			DBGpthread_cond_signal(&condPassengerOnBoard,label);
		}
		
		while (cableWayStatus != CABLEWAY_ARRIVED) {
			DBGpthread_cond_wait(&condCableWayArrived, &mutex, label);
		}

		occupiedSeatByDrunken--;
		if (occupiedSeatByDrunken == 0) {
			DBGpthread_cond_signal(&condPassengerOffBoard, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Il passeggero ubriaco %"PRIiPTR" grida di gioia\n", index);
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condCableWayLeaving, NULL, "cond");
	DBGpthread_cond_init(&condPassengerOnBoard, NULL, "cond");
	DBGpthread_cond_init(&condCableWayArrived, NULL, "cond");
	DBGpthread_cond_init(&condPassengerOffBoard, NULL, "cond");

	/* imposta le condizioni iniziali */
	cableWayStatus = CABLEWAY_ARRIVED;
	occupiedSeatBySober = 0;
	occupiedSeatByDrunken = 0;
	
	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, cableWay, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	for(i = 0; i < NUM_SOBER; i++) {
		rc = pthread_create(&tid, NULL, sober, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	for(i = 0; i < NUM_DRUNKEN; i++) {
		rc = pthread_create(&tid, NULL, drunken, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
