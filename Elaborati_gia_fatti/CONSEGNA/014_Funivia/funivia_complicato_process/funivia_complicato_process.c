/* funivia */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define CABLEWAY_LEAVING 0
#define CABLEWAY_ARRIVED 1

#define NUM_SOBERSEAT 2
#define NUM_DRUNKENSEAT 1

#define NUM_SOBER 4
#define NUM_DRUNKEN 2

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int cableWayStatus;
	int occupiedSeatBySober;
	int occupiedSeatByDrunken;

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condCableWayLeaving;
	pthread_cond_t condPassengerOnBoard;
	pthread_cond_t condCableWayArrived;
	pthread_cond_t condPassengerOffBoard;
} shmo_t;


void *cableWay(shmo_t *shmo, int index)
{
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread cableWay %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->cableWayStatus = CABLEWAY_LEAVING;
		DBGpthread_cond_broadcast(&(shmo->condCableWayLeaving), label);

		if (shmo->occupiedSeatBySober < NUM_SOBERSEAT || shmo->occupiedSeatByDrunken < NUM_DRUNKENSEAT){
			DBGpthread_cond_wait(&(shmo->condPassengerOnBoard), &(shmo->mutex), label);
		} 
		
		printf("I passeggeri a bordo sono %d sobri e %d ubriachi\n", shmo->occupiedSeatBySober, shmo->occupiedSeatByDrunken);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(1);
		printf("La funivia è in cima al campanile\n");
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->cableWayStatus = CABLEWAY_ARRIVED;
		DBGpthread_cond_broadcast(&(shmo->condCableWayArrived), label);

		if (shmo->occupiedSeatBySober != 0 || shmo->occupiedSeatByDrunken != 0){
			DBGpthread_cond_wait(&(shmo->condPassengerOffBoard), &(shmo->mutex), label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void *sober(shmo_t *shmo, int index)
{
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread sober passenger %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->cableWayStatus != CABLEWAY_LEAVING || shmo->occupiedSeatByDrunken != 0 || shmo->occupiedSeatBySober == NUM_SOBERSEAT) {
			DBGpthread_cond_wait(&(shmo->condCableWayLeaving), &(shmo->mutex), label);
		}

		shmo->occupiedSeatBySober++;
		printf("Il passeggero sobrio %d è salito a bordo\n", index);
		if (shmo->occupiedSeatBySober == NUM_SOBERSEAT) {
			DBGpthread_cond_signal(&(shmo->condPassengerOnBoard),label);
		}
		
		while (shmo->cableWayStatus != CABLEWAY_ARRIVED) {
			DBGpthread_cond_wait(&(shmo->condCableWayArrived), &(shmo->mutex), label);
		}

		shmo->occupiedSeatBySober--;
		if (shmo->occupiedSeatBySober == 0) {
			DBGpthread_cond_signal(&(shmo->condPassengerOffBoard), label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		printf("Il passeggero sobrio %d grida di gioia\n", index);
	}
}

void *drunken(shmo_t *shmo, int index)
{
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread drunken passenger %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->cableWayStatus != CABLEWAY_LEAVING || shmo->occupiedSeatBySober != 0 || shmo->occupiedSeatByDrunken == NUM_DRUNKENSEAT) {
			DBGpthread_cond_wait(&(shmo->condCableWayLeaving), &(shmo->mutex), label);
		}

		shmo->occupiedSeatByDrunken++;
		printf("Il passeggero ubriaco %d è salito a bordo\n", index);
		if (shmo->occupiedSeatByDrunken == NUM_DRUNKENSEAT) {
			DBGpthread_cond_signal(&(shmo->condPassengerOnBoard),label);
		}
		
		while (shmo->cableWayStatus != CABLEWAY_ARRIVED) {
			DBGpthread_cond_wait(&(shmo->condCableWayArrived), &(shmo->mutex), label);
		}

		shmo->occupiedSeatByDrunken--;
		if (shmo->occupiedSeatByDrunken == 0) {
			DBGpthread_cond_signal(&(shmo->condPassengerOffBoard), label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		printf("Il passeggero ubriaco %d grida di gioia\n", index);
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->cableWayStatus = CABLEWAY_ARRIVED;
	shmo->occupiedSeatBySober = 0;
	shmo->occupiedSeatByDrunken = 0;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condCableWayLeaving), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condPassengerOnBoard), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condCableWayArrived), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condPassengerOffBoard), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < NUM_SOBER; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			sober(shmo, i);
			exit(0);
		}
	}

	for (i = 0; i < NUM_DRUNKEN; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			drunken(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	cableWay(shmo, i);

	return(0);
}
