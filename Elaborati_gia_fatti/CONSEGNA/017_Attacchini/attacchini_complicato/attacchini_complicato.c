/* attacchini */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMBILLPOSTERS 2
#define NUMGRANDPA 4

/* dati da proteggere */
int numReadyToWork = 0;
int numReading = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condWorkStart;
pthread_cond_t condWorkEnd;
pthread_cond_t condReadEnd;


void *billPoster(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread billPoster %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (numReading != 0) {
			DBGpthread_cond_wait(&condReadEnd, &mutex, label);
		}

		printf("Attacchino%"PRIiPTR" è pronto a lavorare\n", index);
		fflush(stdout);
		if (numReadyToWork == 0) {
			numReadyToWork++;
			DBGpthread_cond_wait(&condWorkStart, &mutex, label);
		}
		else {
			numReadyToWork++;
			DBGpthread_cond_signal(&condWorkStart, label);
		}

		printf("Attacchino%"PRIiPTR" inizia a lavorare\n", index);
		fflush(stdout);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(2);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		numReadyToWork--;
		DBGpthread_cond_broadcast(&condWorkEnd, label);
		printf("Attacchino%"PRIiPTR" ha finito di lavorare\n", index);
		fflush(stdout);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(2);
	}
}

void *grandpa(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alarmClock %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		printf("Vecchio%"PRIiPTR" è pronto a leggere\n", index);
		fflush(stdout);
		while (numReadyToWork != 0) {
			DBGpthread_cond_wait(&condWorkEnd, &mutex, label);
		}
		numReading++;
		printf("Vecchio%"PRIiPTR" inizia a leggere\n", index);
		fflush(stdout);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(3);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		numReading--;
		DBGpthread_cond_broadcast(&condReadEnd, label);
		printf("Vecchio%"PRIiPTR" ha finito di leggere\n", index);
		fflush(stdout);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(1 + (index % 2));
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condWorkStart, NULL, "cond");
	DBGpthread_cond_init(&condWorkEnd, NULL, "cond");
	DBGpthread_cond_init(&condReadEnd, NULL, "cond");

	/* imposta le condizioni iniziali */
	numReadyToWork = 0;
	numReading = 0;

	/* crea i thread */

	for (i = 0; i < NUMGRANDPA; i++) {
		rc = pthread_create(&tid, NULL, grandpa, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	for (i = 0; i < NUMBILLPOSTERS; i++) {
		rc = pthread_create(&tid, NULL, billPoster, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
