/* funivia */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMBILLPOSTERS 2

#define STOP 0
#define START 1

/* dati da proteggere */
int numReadyToWork = 0;
int alarmClockStatus = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condWorkStart;
pthread_cond_t condAlarmClockStart;
pthread_cond_t condAlarmClockStop;


void *billPoster(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread billPoster %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		printf("Attacchino%"PRIiPTR" è pronto a lavorare\n", index);
		if (numReadyToWork == 0) {
			numReadyToWork++;
			DBGpthread_cond_wait(&condWorkStart, &mutex, label);
			alarmClockStatus = START;
			DBGpthread_cond_signal(&condAlarmClockStart, label);
		}
		else {
			DBGpthread_cond_signal(&condWorkStart, label);
		}
		printf("Attacchino%"PRIiPTR" inizia a lavorare\n", index);

		DBGpthread_cond_wait(&condAlarmClockStop, &mutex, label);
		printf("Attacchino%"PRIiPTR" ha finito di lavorare\n", index);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

void *alarmClock(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alarmClock %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (alarmClockStatus != START) {
			DBGpthread_cond_wait(&condAlarmClockStart, &mutex, label);
		}
		printf("Orologio start\n");

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		for (i = 0; i < 5; i++)
		{
			sleep(1);
			printf(".");
			fflush(stdout);
		}
		printf("\n");

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		alarmClockStatus = STOP;
		numReadyToWork = 0;
		DBGpthread_cond_broadcast(&condAlarmClockStop, label);
		printf("Orologio stop\n");

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condWorkStart, NULL, "cond");
	DBGpthread_cond_init(&condAlarmClockStart, NULL, "cond");
	DBGpthread_cond_init(&condAlarmClockStop, NULL, "cond");

	/* imposta le condizioni iniziali */
	numReadyToWork = 0;
	alarmClockStatus = STOP;

	/* crea i thread */

	i = 0;
	rc = pthread_create(&tid, NULL, alarmClock, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	for (i = 0; i < NUMBILLPOSTERS; i++) {
		rc = pthread_create(&tid, NULL, billPoster, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
