/* attacchini */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define NUMBILLPOSTERS 2

#define STOP 0
#define START 1

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int numReadyToWork;
	int alarmClockStatus;

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condWorkStart;
	pthread_cond_t condAlarmClockStart;
	pthread_cond_t condAlarmClockStop;
} shmo_t;

void *billPoster(shmo_t *shmo, int index)
{
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread billPoster %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		printf("Attacchino%d è pronto a lavorare\n", index);
		if (shmo->numReadyToWork == 0) {
			shmo->numReadyToWork++;
			DBGpthread_cond_wait(&(shmo->condWorkStart), &(shmo->mutex), label);
			shmo->alarmClockStatus = START;
			DBGpthread_cond_signal(&(shmo->condAlarmClockStart), label);
		}
		else {
			DBGpthread_cond_signal(&(shmo->condWorkStart), label);
		}
		printf("Attacchino%d inizia a lavorare\n", index);

		DBGpthread_cond_wait(&(shmo->condAlarmClockStop), &(shmo->mutex), label);
		printf("Attacchino%d ha finito di lavorare\n", index);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void *alarmClock(shmo_t *shmo, int index)
{
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alarmClock %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->alarmClockStatus != START) {
			DBGpthread_cond_wait(&(shmo->condAlarmClockStart), &(shmo->mutex), label);
		}
		printf("Orologio start\n");

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		for (i = 0; i < 5; i++)
		{
			sleep(1);
			printf(".");
			fflush(stdout);
		}
		printf("\n");

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->alarmClockStatus = STOP;
		shmo->numReadyToWork = 0;
		DBGpthread_cond_broadcast(&(shmo->condAlarmClockStop), label);
		printf("Orologio stop\n");

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->numReadyToWork = 0;
	shmo->alarmClockStatus = STOP;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condWorkStart), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condAlarmClockStart), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condAlarmClockStop), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < NUMBILLPOSTERS; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			billPoster(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	alarmClock(shmo, i);

	return(0);
}
