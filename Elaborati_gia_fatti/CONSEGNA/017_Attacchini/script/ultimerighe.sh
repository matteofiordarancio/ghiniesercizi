#!/bin/bash

if (( "$#"!="1" )); then
	echo "numero argomenti errato" 1>&2
	exit 1
else
	if [[ -e "$1" ]]; then
		#nohup ./background.sh "$1" >/dev/null 2>&1 &
		(sleep 2; tail -n 3 "$1" >> ./OUTPUT.txt; )
		exit 0
	else
		echo "argomento non file" 1>&2
		exit 2
	fi
fi
