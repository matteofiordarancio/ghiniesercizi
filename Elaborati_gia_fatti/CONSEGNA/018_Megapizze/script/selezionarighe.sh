#!/bin/bash

if (( "$#"!="2" )); then
	echo "numero argomenti errato" 1>&2
	exit 1
else
	if [[ -e "$1" ]]; then
		(sleep 2; cat "$1" | grep "$2" | wc -l >> ./OUTPUT.txt; )
		exit 0
	else
		echo "argomento non file" 1>&2
		exit 2
	fi
fi