/* megapizze */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMPERSON 5
#define NUMSEAT 4

#define EMPTY 0
#define FULL 1

/* dati da proteggere */
int numSeated = 0;
int tableStatus = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condTableEmpty;
pthread_cond_t condTableFull;


void *person(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread person %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		if (numSeated < NUMSEAT && tableStatus != FULL) {
			if (numSeated < NUMSEAT - 1) {
				numSeated++;

				printf("persona%"PRIiPTR" si è seduta\n", index);
				fflush(stdout);

				if (tableStatus != FULL) {
					DBGpthread_cond_wait(&condTableFull, &mutex, label);
				}
			}
			else {
				numSeated++;

				printf("persona%"PRIiPTR" si è seduta\n", index);
				fflush(stdout);

				tableStatus = FULL;
				DBGpthread_cond_broadcast(&condTableFull, label);
				printf("Si mangia!\n");
				fflush(stdout);
			}

			/* rilascia la mutua esclusione */
			DBGpthread_mutex_unlock(&mutex, label);

			sleep(6);

			/* prende la mutua esclusione */
			DBGpthread_mutex_lock(&mutex, label);

			numSeated--;

			printf("persona%"PRIiPTR" si è alzata\n", index);
			fflush(stdout);

			if (numSeated == 0) {
				printf("Il tavolo è libero\n");
				fflush(stdout);
				tableStatus = EMPTY;
				DBGpthread_cond_broadcast(&condTableEmpty, label);
			}

			/* rilascia la mutua esclusione */
			DBGpthread_mutex_unlock(&mutex, label);

			sleep(2 + index);

		}
		else {
			if (tableStatus != EMPTY)
			{
				DBGpthread_cond_wait(&condTableEmpty, &mutex, label);
			}

			/* rilascia la mutua esclusione */
			DBGpthread_mutex_unlock(&mutex, label);
		}
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condTableEmpty, NULL, "cond");
	DBGpthread_cond_init(&condTableFull, NULL, "cond");

	/* imposta le condizioni iniziali */
	numSeated = 0;
	tableStatus = EMPTY;

	/* crea i thread */
	for (i = 0; i < NUMPERSON; i++) {
		rc = pthread_create(&tid, NULL, person, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
