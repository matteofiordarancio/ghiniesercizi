#!/bin/bash

FILENAMELIST=`find /usr/include/linux/ -mindepth 2 -name "*.h" -print`
if [[ -z ${FILENAMELIST} ]] ; then
	echo "nessun file trovato"
else
	for FILENAME in ${FILENAMELIST} ; do
		RESULT=${FILENAME}
		break
	done

	for FILENAME in ${FILENAMELIST} ; do
		if [[ ${RESULT} -ot ${FILENAME} ]] ; then
			RESULT=${FILENAME}
		fi
	done
fi
echo "il file piu' recente è ${RESULT}"
