/* staffetta */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define ALFASIZE 5
#define BETASIZE 4

#define ALFA 0
#define BETA 1

#define OCCUPIED 0
#define VACANT 1


/* dati da proteggere */
int trakStatus = 0;
int groupTurn = 0;
int numAlfaLap[ALFASIZE];
int numBetaLap[BETASIZE];

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condTurnAlfa;
pthread_cond_t condTurnBeta;

int summation(int *array, int size) {
	int i = 0;
	int result = 0;
	for (i = 0; array != NULL && size > 0 && i < size; ++i)
	{
		result = result + array[i];
	}
	return result;
}

void *alfa(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alfa %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (groupTurn != ALFA || numAlfaLap[index] != 0 || trakStatus == OCCUPIED) {
			DBGpthread_cond_wait(&condTurnAlfa, &mutex, label);
		}
		trakStatus = OCCUPIED;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Alfa%"PRIiPTR" inizia\n", index);
		fflush(stdout);
		sleep(1);
		printf("Alfa%"PRIiPTR" finisce\n", index);
		fflush(stdout);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		trakStatus = VACANT;
		numAlfaLap[index]++;
		groupTurn = BETA;

		if (summation(numAlfaLap, ALFASIZE) == ALFASIZE)
		{
			for (i = 0; i < ALFASIZE; ++i)
			{
				numAlfaLap[i] = 0;
			}
		}

		DBGpthread_cond_signal(&condTurnBeta, label);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

void *beta(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread beta %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (groupTurn != BETA || numBetaLap[index] != 0 || trakStatus == OCCUPIED) {
			DBGpthread_cond_wait(&condTurnBeta, &mutex, label);
		}
		trakStatus = OCCUPIED;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Beta%"PRIiPTR" inizia\n", index);
		fflush(stdout);
		sleep(1);
		printf("Beta%"PRIiPTR" finisce\n", index);
		fflush(stdout);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		trakStatus = VACANT;
		numBetaLap[index]++;
		groupTurn = ALFA;

		if (summation(numBetaLap, BETASIZE) == BETASIZE)
		{
			for (i = 0; i < BETASIZE; ++i)
			{
				numBetaLap[i] = 0;
			}
		}

		DBGpthread_cond_signal(&condTurnAlfa, label);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condTurnAlfa, NULL, "cond");
	DBGpthread_cond_init(&condTurnBeta, NULL, "cond");

	/* imposta le condizioni iniziali */
	groupTurn = ALFA;
	for (i = 0; i < ALFASIZE; ++i)
	{
		numAlfaLap[i] = 0;
	}
	for (i = 0; i < BETASIZE; ++i)
	{
		numBetaLap[i] = 0;
	}
	trakStatus = VACANT;

	/* crea i thread */
	for (i = 0; i < ALFASIZE; i++) {
		rc = pthread_create(&tid, NULL, alfa, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	for (i = 0; i < BETASIZE; i++) {
		rc = pthread_create(&tid, NULL, beta, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
