/* conigli */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define INITIALNUMRABBIT 5

#define VACANT 0
#define OCCUPIED 1

/* dati da proteggere */
int numRabbitInTheHole = 0;
int holeStatus = 0;
intptr_t i;
intptr_t couple[2];

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condHoleVacant;
pthread_cond_t condRabbitReady;


void *rabbit(void *arg)
{
	intptr_t index = (intptr_t)arg;
	pthread_t tid;
	int rc;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread %lu"PRIiPTR, (unsigned long int) index);

	printf("È nato Il coniglio%"PRIiPTR"\n", index);
	fflush(stdout);

	sleep(1);

	/* prende la mutua esclusione */
	DBGpthread_mutex_lock(&mutex, label);
	
	while (holeStatus == OCCUPIED) {
		DBGpthread_cond_wait(&condHoleVacant, &mutex, label);
	}

	numRabbitInTheHole++;
	printf("Il coniglio%"PRIiPTR" è nella tana\n", index);
	fflush(stdout);

	if (numRabbitInTheHole == 1) {
		couple[0] = index;
		DBGpthread_cond_wait(&condRabbitReady, &mutex, label);
	} else {
		holeStatus = OCCUPIED;
		couple[1] = index;
		DBGpthread_cond_signal(&condRabbitReady, label);
	}

	numRabbitInTheHole--;

	if (numRabbitInTheHole == 0)
	{
		holeStatus = VACANT;
		DBGpthread_cond_broadcast(&condHoleVacant, label);
		printf("il coniglio%"PRIiPTR" e il coniglio%"PRIiPTR" sono usciti dalla tana\n", couple[0], couple[1]);
		fflush(stdout);
	}

	/* crea il thread */
	rc = pthread_create(&tid, NULL, rabbit, (void*)index);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	/* rilascia la mutua esclusione */
	DBGpthread_mutex_unlock(&mutex, label);

	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int rc;
	/*intptr_t i;*/

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condHoleVacant, NULL, "cond");
	DBGpthread_cond_init(&condRabbitReady, NULL, "cond");

	/* imposta le condizioni iniziali */
	numRabbitInTheHole = 0;
	holeStatus = VACANT;

	/* prende la mutua esclusione */
	DBGpthread_mutex_lock(&mutex, "main");

	/* crea i thread */
	for (i = 0; i < INITIALNUMRABBIT; i++) {
		rc = pthread_create(&tid, NULL, rabbit, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* rilascia la mutua esclusione */
	DBGpthread_mutex_unlock(&mutex, "main");

	pthread_exit(NULL);
	return(0);
}
