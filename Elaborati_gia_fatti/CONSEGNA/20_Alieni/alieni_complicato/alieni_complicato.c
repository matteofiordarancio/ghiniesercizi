/* alieni */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define INITIALNUMALIEN 5
#define NUMALIENSPAWN 2
#define MINALIENINSIDE 1
#define MAXALIENINSIDE 2
#define MINALIENOUTSIDE 2

/* dati da proteggere */
int numDying = 0;
int numOutside = 0;
int numInside = 0;
pthread_t threadIdentifier;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condLast;
pthread_cond_t condOut;
pthread_cond_t condDying;


void *alien(void *arg)
{
	pthread_t tid = pthread_self();
	int rc;
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alien %lu", tid);

	/* prende la mutua esclusione */
	DBGpthread_mutex_lock(&mutex, label);
	
	while (numInside == MAXALIENINSIDE) {
		DBGpthread_cond_wait(&condOut, &mutex, label);
	}

	numInside++;
	numOutside--;
	printf("thread alien %lu è in casa\n", tid);
	fflush(stdout);

	if (numInside == MINALIENINSIDE) {
		DBGpthread_cond_wait(&condLast, &mutex, label);
	} else {
		DBGpthread_cond_signal(&condLast, label);
		DBGpthread_cond_wait(&condOut, &mutex, label);
		if (numInside == MINALIENINSIDE) {
			DBGpthread_cond_wait(&condLast, &mutex, label);
		}
		
	}

	numInside--;
	numOutside++;
	DBGpthread_cond_signal(&condOut, label);

	printf("thread alien %lu è uscito\n", tid);
	fflush(stdout);

	threadIdentifier = tid;
	DBGpthread_cond_signal(&condDying, label);
	
	numDying++;

	/* crea il thread */
	if (numOutside < MINALIENOUTSIDE) {
		printf("thread alien %lu crea %d thread alien\n", tid, NUMALIENSPAWN);
		fflush(stdout);
		for (i = 0; i < NUMALIENSPAWN; i++) {
			rc = pthread_create(&tid, NULL, alien, (void*)NULL);
			if (rc) PrintERROR_andExit(rc, "pthread_create failed");
			numOutside++;
		}
	}
	printf("alieni in casa: %d, alieni fuori: %d, alieni morenti: %d\n", numInside, numOutside, numDying);
	fflush(stdout);

	/* rilascia la mutua esclusione */
	DBGpthread_mutex_unlock(&mutex, label);

	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int  rc;
	int i;
	void *ptr;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condLast, NULL, "cond");
	DBGpthread_cond_init(&condOut, NULL, "cond");
	DBGpthread_cond_init(&condDying, NULL, "cond");

	/* imposta le condizioni iniziali */
	numDying = 0;
	numInside = 0;
	numOutside = INITIALNUMALIEN;

	/* crea i thread */
	for (i = 0; i < INITIALNUMALIEN; i++) {
		rc = pthread_create(&tid, NULL, alien, (void*)NULL);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	while (1) {
		ptr = NULL;

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, "main");
		
		while (numDying == 0) {
			DBGpthread_cond_wait(&condDying, &mutex, "main");
		}

		tid = threadIdentifier;
		numDying--;
		numOutside--;
		
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, "main");

		/* join dei thread creati */
		rc = pthread_join(tid, &ptr);
		if (rc) PrintERROR_andExit(rc, "pthread_join failed");

		printf("main ha rilevato la terminazione del thread alien %lu\n", tid);
		fflush(stdout);
	}

	/* distruzione mutex */
	rc = pthread_mutex_destroy(&mutex);
	if (rc) PrintERROR_andExit(rc, "pthread_mutex_destroy failed");

	/* distruzione cond */
	rc = pthread_cond_destroy(&condLast);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");
	rc = pthread_cond_destroy(&condOut);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");
	rc = pthread_cond_destroy(&condDying);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");

	pthread_exit(NULL);
	return(0);
}
