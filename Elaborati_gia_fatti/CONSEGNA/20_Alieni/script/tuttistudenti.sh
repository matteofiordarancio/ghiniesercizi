#!/bin/bash

FILENAME="email.txt"
exec {FD}<./${FILENAME}
while read -u ${FD} MATRICOLA EMAIL;
do
	if [[ -z "${EMAIL}" || -z "${MATRICOLA}" ]]; then
		exit 1
	fi
	./cercastudente.sh "${EMAIL}" "${MATRICOLA}"
done
# chiusura file
exec {FD}>&-
