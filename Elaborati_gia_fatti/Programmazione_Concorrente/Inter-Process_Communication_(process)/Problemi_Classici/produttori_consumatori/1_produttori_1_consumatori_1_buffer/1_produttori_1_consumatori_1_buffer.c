/* 1 Produttori e 1 Consumatori che si scamsmoiano prodotti mediante un unico Buffer */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smop" /* normalmente /dev/shm */
#define NUMCYCLE 5

/* struttura dello shared memory object */
typedef struct {
	int buffer;
	int occupied;
	pthread_mutex_t mutex;
	pthread_cond_t condFull;
	pthread_cond_t condEmpty;
} shmo_t;

void producer(shmo_t *shmo)
{
	int producedItem = 0;

	while (producedItem <= NUMCYCLE) {
		producedItem++;
		DBGpthread_mutex_lock(&(shmo->mutex), "producer lock");

		while (shmo->occupied >= 1) {
			DBGpthread_cond_wait(&(shmo->condEmpty), &(shmo->mutex), "producer wait");
		}
		assert(shmo->occupied < 1);
		shmo->buffer = producedItem;
		shmo->occupied = 1;
		printf("Il process producer ha riempito il buffer con il dato \"%d\" \n", shmo->buffer);
		fflush(stdout);
		sleep(1);
		DBGpthread_cond_signal(&shmo->condFull, "producer signal");

		DBGpthread_mutex_unlock(&shmo->mutex, "producer unlock");

	}
}

void consumer(shmo_t *shmo)
{
	int consumedItem;

	while (consumedItem <= NUMCYCLE) {
		DBGpthread_mutex_lock(&shmo->mutex, "consumer lock");
		while (shmo->occupied <= 0) {
			DBGpthread_cond_wait(&(shmo->condFull), &(shmo->mutex), "consumer wait");
		}
		assert(shmo->occupied > 0);
		consumedItem = shmo->buffer;
		shmo->occupied = 0;
		printf("Il process consumer ha preso dal buffer il dato \"%d\" \n", consumedItem);
		fflush(stdout);
		sleep(1);
		DBGpthread_cond_signal(&shmo->condEmpty, "consumer signal");

		DBGpthread_mutex_unlock(&shmo->mutex, "consumer unlock");
	}
}

void wait_child_exit(pid_t pid) {
	pid_t childpid;
	int status;

	do {
		childpid = waitpid(pid, &status, 0);
	} while (childpid < 0 && errno == EINTR);
	if (childpid < 0) {
		PrintErrnoAndExit("waitpid failed");
		exit(3);
	}
	else {
		if (WIFEXITED(status)) {
			int rc;
			rc = WEXITSTATUS(status);
			printf("figlio termina restituendo %d\n", rc);
			fflush(stdout);
		}
		else {
			printf("figlio termina in modo anomalo\n");
			fflush(stdout);
		}
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT | O_EXCL | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->occupied = 0;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&shmo->condEmpty, &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&shmo->condFull, &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");

	/* genera un processo figlio */
	pid = fork();
	if (pid < 0) {
		PrintERROR_andExit(errno, "fork failed");
	}
	else if (pid == 0) {
		/* figlio */
		consumer(shmo);
	}
	else {
		/* padre */
		producer(shmo);
		wait_child_exit(pid);
		if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
			perror("shm_unlink failed");
			exit(1);
		}
	}
	return(0);
}
