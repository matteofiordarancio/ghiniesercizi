/* N Produttori e N Consumatori che si scambiano prodotti mediante un unico Buffer */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define NUMPROD 5
#define NUMCONS 3
#define NUMBUFFER 1

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int buffer;
	int numFilledBuffer;
	int numProducerWaiting;
	int numProducerWaitingAndSignalled;
	int numConsumerWaiting;
	int numConsumerWaitingAndSignalled;

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condProducer;
	pthread_cond_t condConsumer;
} shmo_t;

void *producer(shmo_t *shmo, int index)
{
	int producedItem = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "process producer %lu", (unsigned long int) index);

	while (1) {
		/* avviene la produzione */
		producedItem++;

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		/* INIZIO SEZIONE CRITICA */
		/* verifica se deve mettersi in attesa */
		if (shmo->numProducerWaitingAndSignalled >= (NUMBUFFER - shmo->numFilledBuffer)) {
			printf("Il process producer %d entra in attesa \n", index);
			fflush(stdout);
			sleep(1);
			shmo->numProducerWaiting++;
			/* non � il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&(shmo->condProducer), &(shmo->mutex), label);
			shmo->numProducerWaiting--;
			shmo->numProducerWaitingAndSignalled--;
		}
		/* � il turno del thread corrente */
		else {
			printf("Il process producer %d salta l'attesa \n", index);
		}
		/* verifica di coerenza dello stato del buffer */
		if (shmo->numFilledBuffer != 0) {
			printf("Il process producer %d rileva che il buffer � pieno \n", index);
			exit(1);
		}
		/* riempie il buffer col dato prodotto */
		shmo->buffer = producedItem;
		/* aggiorna lo stato dei buffer */
		shmo->numFilledBuffer++;
		printf("Il process producer %d ha riempito il buffer con il dato \"%d\" \n", index, shmo->buffer);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza dello stato del buffer */
		if (shmo->numFilledBuffer == 0) {
			printf("Il process producer %d rileva che il buffer � vuoto \n", index);
			exit(1);
		}
		/* verifica se ci sono thread consumatori in attesa */
		if (
			(shmo->numConsumerWaitingAndSignalled < shmo->numConsumerWaiting)
			&&
			(shmo->numConsumerWaitingAndSignalled < shmo->numFilledBuffer)
			) {
			/* segnala a un process consumer che � il suo turno */
			DBGpthread_cond_signal(&(shmo->condConsumer), label);
			shmo->numConsumerWaitingAndSignalled++;
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void *consumer(shmo_t *shmo, int index)
{
	int consumedItem;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "process consumer %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		/* INIZIO SEZIONE CRITICA */
		/* verifica se deve mettersi in attesa */
		if (shmo->numConsumerWaitingAndSignalled >= shmo->numFilledBuffer) {
			printf("Il process consumer %d entra in attesa \n", index);
			fflush(stdout);
			sleep(1);
			shmo->numConsumerWaiting++;
			/* non � il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&(shmo->condConsumer), &(shmo->mutex), label);
			shmo->numConsumerWaiting--;
			shmo->numConsumerWaitingAndSignalled--;
		}
		/* � il turno del thread corrente */
		else {
			printf("Il process consumer %d salta l'attesa \n", index);
		}
		/* verifica di coerenza dello stato del buffer */
		if (shmo->numFilledBuffer == 0) {
			printf("Il process consumer %d rileva che il buffer � vuoto \n", index);
			exit(1);
		}
		/* prende ci� che c'� nel buffer */
		consumedItem = shmo->buffer;
		/* aggiorna lo stato dei buffer */
		shmo->numFilledBuffer--;
		printf("Il process consumer %d ha preso dal buffer il dato \"%d\" \n", index, consumedItem);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza dello stato del buffer */
		if (shmo->numFilledBuffer != 0) {
			printf("Il process producer %d rileva che il buffer � pieno \n", index);
			exit(1);
		}
		/* verifica se ci sono thread produttori in attesa */
		if (shmo->numProducerWaitingAndSignalled < shmo->numProducerWaiting
			&& shmo->numProducerWaitingAndSignalled < (NUMBUFFER - shmo->numFilledBuffer)) {
			/* segnala a un process producer che � il suo turno */
			DBGpthread_cond_signal(&(shmo->condProducer), label);
			shmo->numProducerWaitingAndSignalled++;
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->numFilledBuffer = 0;
	shmo->numProducerWaiting = 0;
	shmo->numProducerWaitingAndSignalled = 0;
	shmo->numConsumerWaiting = 0;
	shmo->numConsumerWaitingAndSignalled = 0;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condProducer), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condConsumer), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < NUMCONS; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			consumer(shmo, i);
			exit(0);
		}
	}

	for (i = 0; i < NUMPROD; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			producer(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	while (1) {
		sleep(10);
	};

	return(0);
}
