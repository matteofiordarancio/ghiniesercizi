/* Algoritmo del Fornaio */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMTHREADS 4

/* dati da proteggere */
int lastEnqueue = 0;
int nextDequeue = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t  mutexEnqueue;
pthread_mutex_t  mutexDequeue;
pthread_cond_t   condNextToGo;


void *thread(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int queueIndex;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutexEnqueue, label);

		/* INIZIO SEZIONE CRITICA Enqueue */
		/* ottiene il proprio indice nella coda */
		queueIndex = lastEnqueue;
		/* imposta il prossimo indice disponibile della coda */
		lastEnqueue++;
		printf("Il thread %"PRIiPTR" entra in coda alla posizione %d \n", index, queueIndex);
		fflush(stdout);
		sleep(1);
		/* FINE SEZIONE CRITICA Enqueue */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutexEnqueue, label);
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutexDequeue, label);

		/* INIZIO SEZIONE CRITICA Dequeue */
		/* verifica se è il turno del thread corrente */
		while (nextDequeue != queueIndex) {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&condNextToGo, &mutexDequeue, label);
		}
		/* è il turno del thread corrente */
		printf("Il thread %"PRIiPTR" esegue la sua azione \n", index);
		fflush(stdout);
		sleep(1);
		/* imposta il prossimo turno in modo che sia del prossimo thread */
		nextDequeue++;
		/* segnala agli altri thread che è il loro turno */
		DBGpthread_cond_broadcast(&condNextToGo, label);
		printf("Il thread %"PRIiPTR" esce dalla coda in posizione %d \n", index, queueIndex);
		fflush(stdout);
		sleep(1);
		/* FINE SEZIONE CRITICA Dequeue */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutexDequeue, label);

	}

	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutexEnqueue, NULL, "mutex_init");
	DBGpthread_mutex_init(&mutexDequeue, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condNextToGo, NULL, "cond_init");

	/* imposta le condizioni iniziali */
	lastEnqueue = 0;
	nextDequeue = 0;

	/* crea i thread */
	for (i = 0; i < NUMTHREADS; i++) {
		rc = pthread_create(&tid, NULL, thread, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
