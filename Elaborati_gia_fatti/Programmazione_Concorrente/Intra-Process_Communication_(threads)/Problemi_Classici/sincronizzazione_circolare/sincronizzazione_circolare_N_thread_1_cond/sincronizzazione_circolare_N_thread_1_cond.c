/* Sincronizzazione circolare N thread con una condition variable */

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMTHREADS 3

/* dati da proteggere */
int turn = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t cond;

void *thread(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		/*verifica se è il turno del thread corrente */
		while (turn != index) {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&cond, &mutex, label);
			if (turn != index)
				/* non è il turno del thread corrente, quindi il thread corrente segnala al prossimo thread che sarà il suo turno */
				DBGpthread_cond_signal(&cond, label);
		}
		/* è il turno del thread corrente */
		/* imposta il prossimo turno in modo che non sia del thread corrente */
		turn = (turn + 1) % NUMTHREADS;
		printf("Il thread %"PRIiPTR" esegue la sua azione \n", index);
		fflush(stdout);
		sleep(1);
		/* segnala al prossimo thread che sarà il suo turno */
		DBGpthread_cond_signal(&cond, label);
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid[NUMTHREADS];
	int  rc;
	intptr_t i;
	void *ptr;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init failed");

	/* inizializzazione cond */
	DBGpthread_cond_init(&cond, NULL, "cond_init failed");

	/* imposta la condizione iniziale */
	turn = 0;

	/* crea i thread */
	for (i = 0; i < NUMTHREADS; i++) {
		rc = pthread_create(&(tid[i]), NULL, thread, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito, questo che segue non dovrebbe servire */
	/* join dei thread creati */
	for (i = 0; i < NUMTHREADS; i++) {
		rc = pthread_join(tid[i], &ptr);
		if (rc) PrintERROR_andExit(rc, "pthread_join failed");
	}

	/* distruzione mutex */
	rc = pthread_mutex_destroy(&mutex);
	if (rc) PrintERROR_andExit(rc, "pthread_mutex_destroy failed");

	/* distruzione cond */
	rc = pthread_cond_destroy(&cond);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");

	pthread_exit(NULL);
	return(0);
}
