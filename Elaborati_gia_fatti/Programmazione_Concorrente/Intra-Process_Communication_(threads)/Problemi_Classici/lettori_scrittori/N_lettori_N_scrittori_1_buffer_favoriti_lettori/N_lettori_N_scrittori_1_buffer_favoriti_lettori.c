/* N Lettori e N Scrittori su un unico Buffer favorendo i lettori */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMWRITER 5
#define NUMREADER 10

/* dati da proteggere */
int buffer = 0;
char lastTimeAccessedBy = '0';

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;

void *writer(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int producedItem = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread writer %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* avviene la produzione */
		producedItem++;
		/* simula i tempi di produzione */
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		lastTimeAccessedBy = 'w';
		/* riempie il buffer col dato prodotto */
		buffer = producedItem;
		printf("Il thread writer %"PRIiPTR" ha riempito il buffer con il dato \"%d\" \n", index, buffer);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza negli accessi */
		if (lastTimeAccessedBy != 'w') {
			printf("Il thread writer %"PRIiPTR" rileva un incoerenza negli accessi al buffer \n", index);
			fflush(stdout);
			exit(1);
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

void *reader(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int consumedItem;
	int numOfReading = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread reader %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		lastTimeAccessedBy = 'r';
		numOfReading++;
		/* prende ciò che c'è nel buffer */
		consumedItem = buffer;
		printf("Il thread reader %"PRIiPTR" ha preso dal buffer il dato \"%d\" \n", index, consumedItem);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza negli accessi */
		if (lastTimeAccessedBy != 'r') {
			printf("Il thread reader %"PRIiPTR" rileva un incoerenza negli accessi al buffer \n", index);
			fflush(stdout);
			exit(1);
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		/* limita la starvation degli scrittori */
		if (numOfReading % 2 == 0) {
			sleep(2);
		}
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* imposta le condizioni iniziali */
	lastTimeAccessedBy = '0';

	/* crea i thread */
	for (i = 0; i < NUMREADER; i++) {
		rc = pthread_create(&tid, NULL, reader, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	for (i = 0; i < NUMWRITER; i++) {
		rc = pthread_create(&tid, NULL, writer, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
