#!/bin/bash

for name in `find /usr/include/linux/netfilter/ -name "*.h"`; do
	cat $name | grep -q "int" 
	if(($? == 1)); then
		echo $name
	fi
done