#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond;


void * Thread1(void * args) {
	int index = *(int *) args;
	printf("%d\n",index );
	pthread_exit(NULL);
}


int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 4; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,Thread1/*Insert your thread function*/,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}


	pthread_exit(NULL);
}
