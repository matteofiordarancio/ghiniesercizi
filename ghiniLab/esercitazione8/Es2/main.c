/* mutex.c */

#define _THREAD_SAFE

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include "DBGpthread.h"
#define NUMTHRDS 10


int T[3];
int N[3];

pthread_mutex_t mutexBanca1; 
pthread_mutex_t mutexBanca2;
pthread_mutex_t mutexBanca3;

int sum(int a, int b, int c){
	return a+b+c;
}

void * deposito (void *arg){
	intptr_t banca = (intptr_t) arg;
	while(1){
		DBGsleep(1 , "Sleep Deposito");
		if(banca==0){
			DBGpthread_mutex_lock(&mutexBanca1, "lock mutex deposito");
				T[banca]+=10;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca1, "unlock mutex deposito");
		}
		if(banca==1){
			DBGpthread_mutex_lock(&mutexBanca2, "lock mutex deposito");
				T[banca]+=10;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca2, "unlock mutex deposito");
		}
		if(banca==2){
			DBGpthread_mutex_lock(&mutexBanca3, "lock mutex deposito");
				T[banca]+=10;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca3, "unlock mutex deposito");
		}
	}
	pthread_exit(NULL);
}

void * prelievo (void *arg){
	intptr_t banca = (intptr_t) arg;
	while(1){
		DBGsleep(1 , "Sleep Prelievo");
		if(banca==0){
			DBGpthread_mutex_lock(&mutexBanca1, "lock mutex deposito");
				T[banca]-=9;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca1, "unlock mutex deposito");
		}
		if(banca==1){
			DBGpthread_mutex_lock(&mutexBanca2, "lock mutex deposito");
				T[banca]-=9;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca2, "unlock mutex deposito");
		}
		if(banca==2){
			DBGpthread_mutex_lock(&mutexBanca3, "lock mutex deposito");
				T[banca]-=9;
				N[banca]+=1;
				DBGnanosleep(10000, "Sleep in lock deposito");
			DBGpthread_mutex_unlock(&mutexBanca3, "unlock mutex deposito");
		}
	}
	pthread_exit(NULL);
}

void * bancaItalia(void *arg){
	while(1){
		DBGpthread_mutex_lock(&mutexBanca1, "lock mutex bancaItalia");
		DBGpthread_mutex_lock(&mutexBanca2, "lock mutex bancaItalia");
		DBGpthread_mutex_lock(&mutexBanca3, "lock mutex bancaItalia");
			printf("\nBANCHE:\n");
			printf("\t N° Operazioni eseguite: %d\n", sum(N[0], N[1], N[2]));
			printf("\t Totale conti: %d\n", sum(T[0], T[1], T[2]));

			DBGsleep(1, "Wait bank");
		DBGpthread_mutex_unlock(&mutexBanca1, "lock mutex bancaItalia");
		DBGpthread_mutex_unlock(&mutexBanca2, "lock mutex bancaItalia");
		DBGpthread_mutex_unlock(&mutexBanca3, "lock mutex bancaItalia");
		DBGsleep(2, "Wait bank outside");
	}
	pthread_exit(NULL);
}

int main (int argc, char *argv[])
{
	pthread_attr_t attr;
	pthread_t temp;
	intptr_t i;
	int j;

	DBGpthread_mutex_init(&mutexBanca1, NULL, "Inizializzione mutexBanche");
	DBGpthread_mutex_init(&mutexBanca2, NULL, "Inizializzione mutexBanche");
	DBGpthread_mutex_init(&mutexBanca3, NULL, "Inizializzione mutexBanche");
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

	for(i=0;i<3;i++){
		T[i]=0;
		N[i]=0;

		for(j=0; j<5;j++){
			pthread_create(&temp, &attr, deposito, (void *)i); /*gli passo la banca su cui devono lavorare*/
		}
		for(j=0;j<4;j++){	
			pthread_create(&temp, &attr, prelievo, (void *)i);
		}
	}
	pthread_create(&temp, NULL, bancaItalia, NULL);
	pthread_exit(NULL);
}
