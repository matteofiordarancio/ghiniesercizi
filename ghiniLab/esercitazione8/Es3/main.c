/* mutex.c */

#define _THREAD_SAFE

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include "DBGpthread.h"
#define NUMTHRDS 10


int T[3];
int N[3];

pthread_mutex_t swordLock[10]; 


void * fachiro (void * arg){
	int i=0;
	intptr_t fachiro = (intptr_t) arg;
	while(1){
		if(i==10) i=0;
		DBGpthread_mutex_lock(&swordLock[i], "Blocco mutex Fachiro");
		printf("Trafitto fachiro n: %" PRIiPTR " , dalla spada numero %d\n", fachiro, i );
		DBGpthread_mutex_unlock(&swordLock[i], "SBlocco mutex Fachiro");
		i++;
		sleep(1);
	}
	return NULL;
}

int main (int argc, char *argv[])
{
	pthread_attr_t attr;
	pthread_t temp;
	intptr_t i;
	int j;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

	for(j=0; j<10 ; j++){
		DBGpthread_mutex_init(&swordLock[j], NULL, "Inizializzazione mutex");
	}

	i=1;
	pthread_create(&temp, &attr, fachiro, (void *)i);
	i=2;
	pthread_create(&temp, &attr, fachiro, (void *)i);	

	pthread_exit(NULL);
}
