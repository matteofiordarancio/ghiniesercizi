#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t varGlobali;

pthread_cond_t waitForFree;

int occupiedS1[2];
int occupiedS2[4];
int occupiedS3[2];

void * L1(void * args) {
int indexS1taken;
	int indice;
	int indexS2taken;
	int indexS3taken;
	int i;

	if((intptr_t) args == 1){
		indice=1;
	}else{
		indice=2;
	}
	while(1){
		/*comincio a prendere gli strumenti*/	
		DBGpthread_mutex_lock(&varGlobali, "const char *stringMsg");
		while((occupiedS1[0]==1 && occupiedS1[1]==1) || (occupiedS3[0]==1 && occupiedS3[1]==1)){
			printf("Tutti gli sturmenti occupati\n");
			DBGpthread_cond_wait(&waitForFree, &varGlobali, "const char *stringMsg");
		}
		if(occupiedS1[0]==0){
			/*lo prendo*/
			indexS1taken=0;
			occupiedS1[0]=1;
			printf("L1%d prende S10\n",indice);
		}else{
			if(occupiedS1[1]==0){
				indexS1taken=1;
				occupiedS1[1]=1;
			}else{
				printf("SITUAZIONE IMPPOSSIBILEIBLEILSO\n");
			}
		}

		for(i=0;i<4;i++){
			if(occupiedS2[i]==0){
				/*lo prendo*/
				indexS2taken=i;
				occupiedS2[i]=1;
				printf("L1%d prende S2%d\n",indice,i);
				break;
			}
		}

		if(occupiedS3[0]==0){
			/*lo prendo*/
			indexS3taken=0;
			occupiedS3[0]=1;			
			printf("L1%d prende S30\n",indice);
		}else{
			if(occupiedS3[1]==0){
				indexS3taken=1;
				occupiedS3[1]=1;
			}else{
				printf("SITUAZIONE IMPPOSSIBILEIBLEILSO\n");
			}
		}

		DBGpthread_mutex_unlock(&varGlobali, "const char *stringMsg");

		printf("L1%d inizia il suo lavoro visto che ha tutti gli strument\n",indice);
		DBGsleep(5, "eHI");
		printf("L1%d finisce e rilascia gli sturmenti\n",indice);

		DBGpthread_mutex_lock(&varGlobali, "const char *stringMsg");
		printf("L1%d rilasci S1%d S2%d S3%d\n",indice,indexS1taken,indexS2taken,indexS3taken);
		occupiedS1[indexS1taken]=0;
		occupiedS2[indexS2taken]=0;
		occupiedS3[indexS3taken]=0;
		DBGpthread_cond_broadcast(&waitForFree, "const char *stringMsg");
		DBGpthread_mutex_unlock(&varGlobali, "const char *stringMsg");
		sleep(1);
	}
	pthread_exit(NULL);
}

void * L2(void *args){
	int indice;
	int indexS1taken;
	int indexS2taken;
	int indexS3taken;
	int i;

	if((intptr_t) args == 1){
		indice=1;
	}else{
		indice=2;
	}
	while(1){
		/*comincio a prendere gli strumenti*/	
		DBGpthread_mutex_lock(&varGlobali, "const char *stringMsg");
		while((occupiedS1[0]==1 && occupiedS1[1]==1) || (occupiedS3[0]==1 && occupiedS3[1]==1)){
			printf("Tutti gli sturmenti occupati\n");
			DBGpthread_cond_wait(&waitForFree, &varGlobali, "const char *stringMsg");
			printf("L2%d si sblocca\n",indice);
		}
		if(occupiedS1[0]==0){
			/*lo prendo*/
			indexS1taken=0;
			occupiedS1[0]=1;
			printf("L2%d prende S10\n",indice);
		}else{
			if(occupiedS1[1]==0){
				indexS1taken=1;
				occupiedS1[1]=1;
			}else{
				printf("SITUAZIONE IMPPOSSIBILEIBLEILSO\n");
			}
		}

		for(i=0;i<4;i++){
			if(occupiedS2[i]==0){
				/*lo prendo*/
				indexS2taken=i;
				occupiedS2[i]=1;
				printf("L2%d prende S2%d\n",i,indice);
				break;
			}
		}

		if(occupiedS3[0]==0){
			/*lo prendo*/
			indexS3taken=0;
			occupiedS3[0]=1;			
			printf("L2%d prende S10\n",indice);
		}else{
			if(occupiedS3[1]==0){
				indexS3taken=1;
				occupiedS3[1]=1;
			}else{
				printf("SITUAZIONE IMPPOSSIBILEIBLEILSO\n");
			}
		}

		DBGpthread_mutex_unlock(&varGlobali, "const char *stringMsg");

		printf("L2%d inizia il suo lavoro visto che ha tutti gli strument\n",indice);
		DBGsleep(2, "eHI");
		printf("L2%d finisce e rilascia gli sturmenti\n",indice);

		DBGpthread_mutex_lock(&varGlobali, "const char *stringMsg");
		printf("L2%d rilasci S1%d S2%d S3%d\n",indice,indexS1taken,indexS2taken,indexS3taken);
		occupiedS1[indexS1taken]=0;
		occupiedS2[indexS2taken]=0;
		occupiedS3[indexS3taken]=0;
		DBGpthread_cond_broadcast(&waitForFree, "const char *stringMsg");	
		DBGpthread_mutex_unlock(&varGlobali, "const char *stringMsg");
	}
	pthread_exit(NULL);
}

int main() {
	pthread_t th;
	intptr_t i;
	DBGpthread_mutex_init(&varGlobali, NULL, "Error");
	DBGpthread_cond_init(&waitForFree, NULL, "const char *stringMsg");


	i=1;	 
	if (pthread_create( &th,NULL,L1,(void*)i)) {
		PrintERROR_andExit(1, "Error");
	}
	i=2;
	if (pthread_create( &th,NULL,L1,(void*)i)) {
		PrintERROR_andExit(1, "Error");
	}

	i=1;
	if (pthread_create( &th,NULL,L2,(void*)i)) {
		PrintERROR_andExit(1, "Error");
	}
	i=2;
	if (pthread_create( &th,NULL,L2,(void*)i)) {
		PrintERROR_andExit(1, "Error");
	}

	pthread_exit(NULL);
}
