#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond;
int nThread=0;

void * Thread1(void * args) {
	pthread_t th;
	while(1){
		pthread_mutex_lock(&mutex);
		if(nThread>4){
			nThread--;
			pthread_mutex_unlock(&mutex);
			pthread_exit(NULL);
		}else{
			if (pthread_create( &th,NULL,Thread1,NULL)) {
				PrintERROR_andExit(1, "Error");
			}
			nThread++;
		}
		printf("IL numero di thread è %d\n",nThread);
		fflush(stdout);
		pthread_mutex_unlock(&mutex);
	}
	sleep(1);
}


int main() {
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond, NULL, "Error");

	while(1){
		if (pthread_create( &th,NULL,Thread1,NULL)) {
			PrintERROR_andExit(1, "Error");
		}
		pthread_mutex_lock(&mutex);
		nThread++;
		pthread_mutex_unlock(&mutex);
		DBGsleep(5, "EH Y");
	}
	pthread_exit(NULL);
}
