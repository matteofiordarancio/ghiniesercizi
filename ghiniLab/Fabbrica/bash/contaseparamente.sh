#!/bin/bash

if(($#>9)); then
	exit 1
fi

i=0
sommaPari=0
sommaDispari=0
for name in "$@"; do
	if(( i % 2 == 0)); then
		sommaPari=$(($sommaPari + `cat "$name"| wc -l`))
	else
		sommaDispari=$(($sommaDispari + `cat "$name"| wc -l`))
	fi
	i=$(($i+1))
done

echo $sommaPari
echo $sommaDispari >&2