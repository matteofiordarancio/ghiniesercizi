/* file:  NProd_NCons.c 
   NProduttori e NConsumatori che si scambiano prodotti
   mediante un unico Buffer. E' la versione efficente perche' 
   - utilizza due pthread_cond_var (una per Prod e una per Cons)
   - sveglia i pthread solo quando hanno qualcosa da fare.
*/ 

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif


#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include "printerror.h"
#include "DBGpthread.h"

#define NUMPRODA 4
#define NUMPRODB 5
#define NUMCONSA 6
#define NUMCONSB 4 
#define NUMBUFFER 1	/* NON MODIFICARE MAI */

/* dati da proteggere */
uint64_t valGlobale=0;

/* variabili per la sincronizzazione */
pthread_mutex_t  mutex; 
pthread_cond_t   condProd, condCons; 
int numBufferPieni=0; /* 0 o 1 */
int numProdWaiting=0;
int numProdWaitingAndSignalled=0; /* deve essere <= #buffer vuoti */
int numConsWaiting=0;
int numConsWaitingAndSignalled=0; /* deve essere <= #buffer pieni */

int isA=-1;

void *ProduttoreA (void *arg) 
{ 
	char Plabel[128];
	char Plabelsignal[128];
	int indice;
	uint64_t valProdotto=0;

	indice=*((int*)arg);
	free(arg);
	sprintf(Plabel,"P%d",indice);
	sprintf(Plabelsignal,"P%d->C",indice);

	while(1) {
		valProdotto++;
		
		DBGpthread_mutex_lock(&mutex,Plabel); 
		if( numProdWaitingAndSignalled >= (NUMBUFFER-numBufferPieni)){
			numProdWaiting++;
			while(isA!=-1){
				DBGpthread_cond_wait(&condProd,&mutex,Plabel);
			}
			numProdWaiting--;
			numProdWaitingAndSignalled--;
		}
		valGlobale=valProdotto;
		isA=1;

		printf("ProdA %s inserisce %lu \n", Plabel, valGlobale ); 
		fflush(stdout);

		numBufferPieni++;

		if( 
			( numConsWaitingAndSignalled < numConsWaiting ) 
			&&
			( numConsWaitingAndSignalled < numBufferPieni ) 
		  ) {
			/* risveglio un Consumatore per svuotare un buffer */
			DBGpthread_cond_broadcast(&condCons,Plabelsignal); 
			numConsWaitingAndSignalled++;
		}
		DBGnanosleep(50000000,"sleeping");
		/* rilascio mutua esclusione */
		DBGpthread_mutex_unlock(&mutex,Plabel); 
	}
	pthread_exit(NULL); 
}

void *ProduttoreB (void *arg) 
{ 
	char Plabel[128];
	char Plabelsignal[128];
	int indice;
	uint64_t valProdotto=0;

	indice=*((int*)arg);
	free(arg);
	sprintf(Plabel,"P%d",indice);
	sprintf(Plabelsignal,"P%d->C",indice);

	while(1) {
		valProdotto++;
		
		DBGpthread_mutex_lock(&mutex,Plabel);
		if( numProdWaitingAndSignalled >= (NUMBUFFER-numBufferPieni)){
			numProdWaiting++;
			while(isA!=-1){
				DBGpthread_cond_wait(&condProd,&mutex,Plabel);
			}
			numProdWaiting--;
			numProdWaitingAndSignalled--;
		}
		valGlobale=valProdotto;
		isA=0;

		printf("ProdB %s inserisce %lu \n", Plabel, valGlobale ); 
		fflush(stdout);

		numBufferPieni++;

		if( 
			( numConsWaitingAndSignalled < numConsWaiting ) 
			&&
			( numConsWaitingAndSignalled < numBufferPieni ) 
		  ) {
			/* risveglio un Consumatore per svuotare un buffer */
			DBGpthread_cond_broadcast(&condCons,Plabelsignal); 
			numConsWaitingAndSignalled++;
		}
		DBGnanosleep(50000000,"sleeping");
		/* rilascio mutua esclusione */
		DBGpthread_mutex_unlock(&mutex,Plabel); 
	}
	pthread_exit(NULL); 
}  

void *ConsumatoreA (void *arg) 
{ 
	uint64_t val;
	char Clabel[128];
	char Clabelsignal[128];
	int indice;

	indice=*((int*)arg);
	free(arg);
	sprintf(Clabel,"C%d",indice);
	sprintf(Clabelsignal,"C%d->P",indice);

	
	while(1) {
		DBGpthread_mutex_lock(&mutex,Clabel); 

		if( numConsWaitingAndSignalled >= numBufferPieni ) {
			numConsWaiting++;
			while(isA!=1){
				DBGpthread_cond_wait(&condCons,&mutex,Clabel);
			}
			numConsWaiting--;
			numConsWaitingAndSignalled--;
		}

		/* prendo cio' che c'e' nel buffer */
		val=valGlobale;
		isA=-1;

		printf("ConsA %s legge %lu \n", Clabel, val ); 
		fflush(stdout);

		numBufferPieni--;
		if( 
		    (numProdWaitingAndSignalled < numProdWaiting ) 
			&&
		    (numProdWaitingAndSignalled < (NUMBUFFER-numBufferPieni)) 
		  ) 
		{ 
			/* risveglio 1 Prod per riempire il buffer svuotato */
			DBGpthread_cond_broadcast(&condProd,Clabelsignal); 
			numProdWaitingAndSignalled++;
		}
		DBGnanosleep(50000000,"sleeping");
		/* rilascio mutua esclusione */
		DBGpthread_mutex_unlock(&mutex,Clabel); 
	}
	pthread_exit(NULL); 
}

void *ConsumatoreB (void *arg) 
{ 
	uint64_t val;
	char Clabel[128];
	char Clabelsignal[128];
	int indice;

	indice=*((int*)arg);
	free(arg);
	sprintf(Clabel,"C%d",indice);
	sprintf(Clabelsignal,"C%d->P",indice);

	
	while(1) {
		DBGpthread_mutex_lock(&mutex,Clabel); 

		if( numConsWaitingAndSignalled >= numBufferPieni ) {
			numConsWaiting++;
			while(isA!=0){
				DBGpthread_cond_wait(&condCons,&mutex,Clabel);
			}
			numConsWaiting--;
			numConsWaitingAndSignalled--;
		}

		/* prendo cio' che c'e' nel buffer */
		val=valGlobale;
		isA=-1;

		printf("ConsB %s legge %lu \n", Clabel, val ); 
		fflush(stdout);

		numBufferPieni--;
		if( 
		    (numProdWaitingAndSignalled < numProdWaiting ) 
			&&
		    (numProdWaitingAndSignalled < (NUMBUFFER-numBufferPieni)) 
		  ) 
		{ 
			/* risveglio 1 Prod per riempire il buffer svuotato */
			DBGpthread_cond_broadcast(&condProd,Clabelsignal); 
			numProdWaitingAndSignalled++;
		}
		DBGnanosleep(50000000,"sleeping");
		/* rilascio mutua esclusione */
		DBGpthread_mutex_unlock(&mutex,Clabel); 
	}
	pthread_exit(NULL); 
}  

int main (int argc, char* argv[] ) 
{ 
	pthread_t    th; 
	int  i, *intptr;

	DBGpthread_cond_init(&condProd, NULL,"pthread_cond_init failed");
	DBGpthread_cond_init(&condCons, NULL,"pthread_cond_init failed");
	DBGpthread_mutex_init(&mutex,NULL,"pthread_mutex_init failed");

	/* all'inizio i Cons devono aspettare il primo Prod */
	numBufferPieni=0; /* 0 o 1 */
	numProdWaiting=0;
	numProdWaitingAndSignalled=0;
	numConsWaiting=0;
	numConsWaitingAndSignalled=0;

	for(i=0;i<NUMCONSA;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ConsumatoreA,(void*)intptr);
	}

	for(i=0;i<NUMPRODA;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ProduttoreA,(void*)intptr); 
	}

	for(i=0;i<NUMCONSB;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ConsumatoreB,(void*)intptr);
	}

	for(i=0;i<NUMPRODB;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ProduttoreB,(void*)intptr); 
	}

	pthread_exit(NULL); 

	return(0); 
} 
  
  
  
