#!/bin/bash

( PIDBASHGRUPPO=${BASHPID}; tail -n 100000 --pid=${PIDBASHGRUPPO} -f 7.txt |
 while read RIGA ; do ((NUM=${NUM}+1)); if [[ ${RIGA} == "k" ]] ; then
echo NUM RIGHE ${NUM} ; kill -SIGKILL ${PIDBASHGRUPPO}; exit 0; fi ;
done )