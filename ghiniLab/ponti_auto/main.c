/* file:  algoritmo_fornaio_per_coda_FIFO.c
	  i clienti accedono da soli al bancone del pane
	  nell'ordine con cui hanno ottenuto il biglietto.
*/ 

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif


#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>	/* uint64_t intptr_t */
#include <inttypes.h>	/* uint64_t  PRIiPTR */
#include <sys/time.h>	/* gettimeofday()    struct timeval */
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"


#define NUMAUTO 4 
#define ORARIO 1
#define ANTIORARIO 0

/* variabili da proteggere */
int64_t bigliettoGlob=0;
int64_t senso_marcia_ponte = -1;
int bigliettiMacchineORARIO[4];
int bigliettiMacchineANTI[4];

int numeroAutoAttesaORARIO=0;
int numeroAutoAttesaANTI = 0;
/* variabili per la sincronizzazione */
pthread_mutex_t  mutexDistributoreBigliettiORARIO;
pthread_mutex_t  mutexDistributoreBigliettiANTI;
pthread_mutex_t  mutexControlloVarGlobali;
pthread_cond_t   cond_car_ORARIO;
pthread_cond_t   cond_car_ANTI;

int trovaMinORARIO(){
	int i;
	int id_min;
	int min=5000;
	for(i=0;i<4;i++){
		if(bigliettiMacchineORARIO[i]<min){
			min = bigliettiMacchineORARIO[i];
			id_min = i;
		}
	}
	return id_min;
}
int trovaMinANTI(){
	int i;
	int id_min;
	int min=5000;
	for(i=0;i<4;i++){
		if(bigliettiMacchineANTI[i]<min){
			min = bigliettiMacchineANTI[i];
			id_min = i;
		}
	}
	return id_min;
}

void *macchinaORARIO (void *arg) 
{ 
	int id = *((int *) arg);
	while(1) 
	{	
		/*bigliettiMacchineORARIO[id] non ha bisogno della lock perché ogni processo ne ha una*/
		if(bigliettiMacchineORARIO[id]==4999){ /*non ho ancora preso il biglietto*/
			DBGpthread_mutex_lock(&mutexDistributoreBigliettiORARIO,"Biglietto"); 
			bigliettiMacchineORARIO[id] = bigliettoGlob;
			bigliettoGlob++;
			numeroAutoAttesaORARIO++;
			/* rilascio la mutua esclusione sul distributore */
			DBGpthread_mutex_unlock(&mutexDistributoreBigliettiORARIO,"Biglietto");
		}

		DBGpthread_mutex_lock(&mutexControlloVarGlobali, "BLOCCO");
		while(senso_marcia_ponte == ANTIORARIO){ /*il senso è l'opposto quindi mi metto in attesa di venire svegliato*/
			printf("ORARIO car: %d si mette in attesa\n", id);
			DBGpthread_cond_wait(&cond_car_ORARIO, &mutexControlloVarGlobali, "Attesa senso antiorario");
		}
		if(senso_marcia_ponte == ORARIO){
			/* il senso è il mio ed è già passata metà*/
			/*passo*/
			DBGsleep(1, "sleep passaggio metà");
			printf("ORARIO car: %d biglietto: %d ha raggiunto la propria metà\n", id, bigliettiMacchineORARIO[id]);
			bigliettiMacchineORARIO[id] = 4999; /*devo riprendere un nuovo biglietto */
			numeroAutoAttesaORARIO--;
			DBGpthread_cond_broadcast(&cond_car_ORARIO, "Risveglio tutti orario");  /*sveglio quelli del mio senso se non sono già svegli*/
			if(numeroAutoAttesaORARIO <= 0){ /*se non c'è più nessuno in attesa*/
				/*passo e poi libero il ponte*/
				DBGsleep(1, "sleep passaggio seconda metà");
				senso_marcia_ponte = -1;
				printf("ORARIO car: %d era l'ultimo in questo senso quindi sveglia tutti\n", id);
				DBGpthread_cond_broadcast(&cond_car_ANTI, "Risveglio proprio tutti");
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO"); 
			}else{
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
				DBGsleep(1, "sleep passaggio seconda metà");
				printf("ORARIO car: %d ha oltrepassato il ponte\n", id); 
			}
			DBGsleep(10, "sleep giro intorno la city");
		}else{
			/*il ponte è vuoto, controllo le regole*/
			if(numeroAutoAttesaORARIO >= numeroAutoAttesaANTI){
				/*controllo di poter passare */
				if(id == trovaMinORARIO()){ /*sono io effettivamente a dover passare, quindi prendo il passaggio sul ponte*/
					senso_marcia_ponte = ORARIO;
					DBGsleep(1, "sleep passaggio metà");
					printf("ORARIO FIRST car: %d biglietto: %d ha raggiunto la propria metà\n", id, bigliettiMacchineORARIO[id]);
					bigliettiMacchineORARIO[id] = 4999; /*devo riprendere un nuovo biglietto */
					numeroAutoAttesaORARIO--;
					DBGpthread_cond_broadcast(&cond_car_ORARIO, "Risveglio tutti orario");  /*sveglio quelli del mio senso se non sono già svegli*/
					if(numeroAutoAttesaORARIO <= 0){ /*se non c'è più nessuno in attesa*/
						/*passo e poi libero il ponte*/
						DBGsleep(1, "sleep passaggio seconda metà");
						senso_marcia_ponte = -1;
						printf("ORARIO FIRST car: %d era l'ultimo in questo senso quindi sveglia tutti\n", id);
						DBGpthread_cond_broadcast(&cond_car_ANTI, "Risveglio proprio tutti");
						DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO"); 
					}else{
						DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
						DBGsleep(1, "sleep passaggio seconda metà");
						printf("ORARIO FIRST car: %d ha oltrepassato il ponte\n", id); 
					}
					DBGsleep(10, "sleep giro intorno la city");
				}else{
					/*non sono io il target, quindi rilascio la lock aspettando la prende la giusta auto */
					DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
				}
			}else{
				/*in senso antiorario sono di più, quindi rilascio la lock aspettando la prenda un antiorario*/
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
			}
		}
	}

	pthread_exit(NULL); 
} 

void *macchinaANTI (void *arg){

	int id = *((int *) arg);
	while(1) 
	{
		if(bigliettiMacchineANTI[id]==4999){ /*non ho ancora preso il biglietto*/
			DBGpthread_mutex_lock(&mutexDistributoreBigliettiANTI,"Biglietto"); 
			bigliettiMacchineANTI[id] = bigliettoGlob;
			bigliettoGlob++;
			numeroAutoAttesaANTI++;
			/* rilascio la mutua esclusione sul distributore */
			DBGpthread_mutex_unlock(&mutexDistributoreBigliettiANTI,"Biglietto");
		}

		DBGpthread_mutex_lock(&mutexControlloVarGlobali, "BLOCCO");
		while(senso_marcia_ponte == ORARIO){ /*il senso è l'opposto quindi mi metto in attesa di venire svegliato*/
			printf("ANTIORARIO car: %d si mette in attesa\n", id);
			DBGpthread_cond_wait(&cond_car_ANTI, &mutexControlloVarGlobali, "Attesa senso orario");
		}
		if(senso_marcia_ponte == ANTIORARIO){
			/* il senso è il mio ed è già passata metà*/
			/*passo*/
			DBGsleep(1, "sleep passaggio metà");
			printf("ANTIORARIO car: %d biglietto: %d ha raggiunto la propria metà\n", id, bigliettiMacchineANTI[id]);
			bigliettiMacchineANTI[id] = 4999; /*devo riprendere un nuovo biglietto */
			numeroAutoAttesaANTI--;
			DBGpthread_cond_broadcast(&cond_car_ANTI, "Risveglio tutti antiorario");  /*sveglio quelli del mio senso se non sono già svegli*/
			if(numeroAutoAttesaANTI <= 0){ /*se non c'è più nessuno in attesa*/
				/*passo e poi libero il ponte*/
				DBGsleep(1, "sleep passaggio seconda metà");
				senso_marcia_ponte = -1;
				printf("ANTIORARIO car: %d era l'ultimo in questo senso quindi sveglia tutti\n", id);
				DBGpthread_cond_broadcast(&cond_car_ORARIO, "Risveglio proprio tutti");
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO"); 
			}else{
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
				DBGsleep(1, "sleep passaggio seconda metà");
				printf("ANTIORARIO car: %d ha oltrepassato il ponte\n", id); 
			}
			DBGsleep(10, "sleep giro intorno la city");
		}else{
			/*il ponte è vuoto, controllo le regole*/
			if(numeroAutoAttesaANTI > numeroAutoAttesaORARIO){
				/*controllo di poter passare */
				if(id == trovaMinANTI()){ /*sono io effettivamente a dover passare, quindi prendo il passaggio sul ponte*/
					senso_marcia_ponte = ANTIORARIO;
					DBGsleep(1, "sleep passaggio metà");
					printf("ANTIORARIO FIRST car: %d biglietto: %d ha raggiunto la propria metà\n", id, bigliettiMacchineANTI[id]);
					bigliettiMacchineANTI[id] = 4999; /*devo riprendere un nuovo biglietto */
					numeroAutoAttesaANTI--;
					DBGpthread_cond_broadcast(&cond_car_ANTI, "Risveglio tutti antiorario");  /*sveglio quelli del mio senso se non sono già svegli*/
					if(numeroAutoAttesaANTI <= 0){ /*se non c'è più nessuno in attesa*/
						/*passo e poi libero il ponte*/
						DBGsleep(1, "sleep passaggio seconda metà");
						senso_marcia_ponte = -1;
						printf("ANTIORARIO FIRST car: %d era l'ultimo in questo senso quindi sveglia tutti\n", id);
						DBGpthread_cond_broadcast(&cond_car_ORARIO, "Risveglio proprio tutti");
						DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO"); 
					}else{
						DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
						DBGsleep(1, "sleep passaggio seconda metà");
						printf("ANTIORARIO FIRST car: %d ha oltrepassato il ponte\n", id); 
					}
					DBGsleep(10, "sleep giro intorno la city");
				}else{
					/*non sono io il target, quindi rilascio la lock aspettando la prende la giusta auto */
					DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
				}
			}else{
				/*in senso antiorario sono di più, quindi rilascio la lock aspettando la prenda un antiorario*/
				DBGpthread_mutex_unlock(&mutexControlloVarGlobali, "BLOCCO");
			}
		}
	}

	pthread_exit(NULL); 
}

int main (int argc, char* argv[] ) 
{ 
	pthread_t   th; 
	int i;
	int *ptr;
	DBGpthread_mutex_init( &mutexDistributoreBigliettiORARIO, NULL,"pthread_mutex_init failed");
	DBGpthread_mutex_init( &mutexDistributoreBigliettiANTI, NULL,"pthread_mutex_init failed");
	DBGpthread_mutex_init( &mutexControlloVarGlobali, NULL,"pthread_mutex_init failed");
	DBGpthread_cond_init( &cond_car_ORARIO, NULL,"pthread_cond_init failed");
	DBGpthread_cond_init( &cond_car_ANTI, NULL,"pthread_cond_init failed");

	
	/* lancio i clienti */
	for(i=0;i<NUMAUTO;i++) {
		bigliettiMacchineORARIO[i]=4999;
		ptr = malloc(sizeof(int));
		*ptr = i;
		pthread_create( &th, NULL,macchinaORARIO,(void*)ptr); 
	}

	for(i=0;i<NUMAUTO;i++) {	
		bigliettiMacchineANTI[i]=4999;
		ptr = malloc(sizeof(int));
		*ptr = i;
		pthread_create( &th, NULL,macchinaANTI,(void*)ptr); 
	}

	pthread_exit(NULL); 

	return(0); 
} 
  
  
  
