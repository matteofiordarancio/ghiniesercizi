#!/bin/bash
rm -rf "output.txt"
exec   {FD1}<  "cadutevic.txt"
while read -u ${FD1} A B C D  ; do
	echo $C >> "output.txt"
done

sort -u "output.txt" > "unique.txt"
exec   {FD2}<  "unique.txt"

while read -u ${FD2} parola  ; do
	echo $parola" "`cat output.txt | grep $parola | wc -l`
done
rm -rf "unique.txt"