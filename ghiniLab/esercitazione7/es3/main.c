/* banale_contrucco.c  */

 
#define _THREAD_SAFE
#define _REENTRANT
#define _DEFAULT_SOURCE


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>	/* uintptr_t */
#include <inttypes.h>	/* PRIiPTR */
#include <pthread.h>
#include <string.h>


struct s{
	int N;
	char Str[100];
	int indice;
};
typedef struct s Struttura;

void * func(void * input){
	Struttura *p;
	Struttura *insideFor;
	pthread_t *arr;
	int i;


	p= (Struttura *) input;
	printf("inizio thread N %i  Indice %i \n", p->N, p->indice );
	fflush(stdout);
	sleep(1);
	if(p->N>1){
		arr=malloc(sizeof(pthread_t)*(p->N-1));
		for(i=0; i<p->N-1; i++){
			insideFor = malloc(sizeof(Struttura));
			insideFor->N = p->N-1;
			insideFor->indice = p->indice-i-1;
			pthread_create(&arr[i], NULL, func, (void *) insideFor);
		}
		for(i=0;i<p->N-1;i++){
			pthread_join(arr[i], (void **) &insideFor);
			printf("received \"%s\"\n", insideFor->Str);
			fflush(stdout);
			free(insideFor);
		}
	}
	sprintf(p->Str, "%i %i", p->N, p->indice);
	pthread_exit(p);
}


int main()
{
	Struttura *p;
	pthread_t t;

	p=malloc(sizeof(Struttura));
	p->N =3;
	p->indice = 10;
	strcpy(p->Str,"-1 -1");

	pthread_create(&t, NULL, func, (void *) p);
	pthread_join(t, NULL);

	pthread_exit(NULL);
}