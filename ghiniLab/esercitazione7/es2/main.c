/* banale_contrucco.c  */

 
#define _THREAD_SAFE
#define _REENTRANT
#define _BSD_SOURCE


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>	/* uintptr_t */
#include <inttypes.h>	/* PRIiPTR */
#include <pthread.h>
#include <string.h>

#define NUM_THREADS 100

int N;
char buf[500];

void *PrintHello(void *arg)
{
	pthread_t tid;
	intptr_t temp;
	usleep(1000);
	printf("\n %"  PRIiPTR , (intptr_t)arg);
	temp=(intptr_t)arg +1;
	N=pthread_create(&tid, NULL, PrintHello, (void *) temp);
	if(N){
		printf("\n\n\nReached end of possibile pthread\n\n\n");
		strerror_r(N, buf, 500);
		printf("%s",buf);
		pthread_exit(NULL);
	}
	pthread_join(tid, NULL);
	pthread_exit(NULL);
}

int main()
{
	pthread_t tid;
	intptr_t i=0;
	pthread_create(&tid, NULL, PrintHello, (void *) i);
	pthread_join(tid, NULL);
	pthread_exit(NULL);

}