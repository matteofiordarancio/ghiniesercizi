#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex_bigliettoOrario;
pthread_mutex_t mutex_bigliettoAntiorario;
pthread_mutex_t mutex;
pthread_cond_t cond_waitOrario;
pthread_cond_t cond_waitAntiOrario;

int bigliettoGlobaleOrario=0;
int bigliettoGlobaleAntiOrario=0;
int numeroAutoOrario=0;
int numeroAutoAntiOrario=0;
int ponteStatus; /*0 vuoto, 1 orario, 2 antiorario*/
int hasCrossedHalf=0;

int bigliettiOrari[4];
int bigliettiAntiOrari[4];

/*da usare in mutex*/
int minBigliettoAntiOrario(){
	int i;
	int min=bigliettiAntiOrari[0];
	for(i = 1; i < 4; i++) {
		if(bigliettiAntiOrari[i]>min){
			min = bigliettiAntiOrari[i];
		}
	}
	return min;
}

/*da usare in mutex*/
int verificaCondizioniAntiOrario(int biglietto){
	if(ponteStatus==1){
		return 1;
	}
	if(ponteStatus==0){
		pthread_mutex_lock(&mutex_bigliettoOrario);
		pthread_mutex_lock(&mutex_bigliettoAntiorario);
		if(numeroAutoAntiOrario<=numeroAutoOrario) return 1;
		pthread_mutex_unlock(&mutex_bigliettoAntiorario);
		pthread_mutex_unlock(&mutex_bigliettoOrario);

		if(biglietto != minBigliettoAntiOrario()) return 1;
	}
	if(ponteStatus==2){
		if(hasCrossedHalf==0) return 1;
	}
	return 0;
}

/*da usare in mutex*/
int minBigliettoOrario(){
	int i;
	int min=bigliettiOrari[0];
	for(i = 1; i < 4; i++) {
		if(bigliettiOrari[i]>min){
			min = bigliettiOrari[i];
		}
	}
	return min;
}

/*da usare in mutex*/
int verificaCondizioniOrario(int biglietto){
	if(ponteStatus==2){
		return 1;
	}
	if(ponteStatus==0){
		pthread_mutex_lock(&mutex_bigliettoOrario);
		pthread_mutex_lock(&mutex_bigliettoAntiorario);
		if(numeroAutoOrario<numeroAutoAntiOrario) return 1;
		pthread_mutex_unlock(&mutex_bigliettoAntiorario);
		pthread_mutex_unlock(&mutex_bigliettoOrario);

		if(biglietto != minBigliettoOrario()) return 1;
	}
	if(ponteStatus==1){
		if(hasCrossedHalf==0) return 1;
	}
	return 0;
}

void * orario(void * args) {
	int index = *(int *) args;

	while(1){
		/*biglietto*/
		pthread_mutex_lock(&mutex_bigliettoOrario);
		bigliettiOrari[index]=bigliettoGlobaleOrario;
		bigliettoGlobaleOrario++;
		numeroAutoOrario++;
		printf("Orario %d prende biglietto %d\n",index,bigliettiOrari[index]);
		pthread_mutex_unlock(&mutex_bigliettoOrario);
		
		pthread_mutex_lock(&mutex);
		while(verificaCondizioniOrario(bigliettiOrari[index])){ /*condizioni per cui non posso passare */
			DBGpthread_cond_wait(&cond_waitAntiOrario, &mutex, "const char *stringMsg");
		}
		/*sono sciuro possa passare*/
		ponteStatus=1; /*sta passando un orario*/
		hasCrossedHalf=0;
		pthread_mutex_lock(&mutex_bigliettoOrario);
		numeroAutoOrario--;
		pthread_mutex_unlock(&mutex_bigliettoOrario);
		printf("Orario %d con biglietto %d inizia a passare sul ponte\n",index, bigliettiOrari[index]);
		pthread_mutex_unlock(&mutex);
		
		DBGsleep(1, "const char *stringMsg");

		pthread_mutex_lock(&mutex);
		hasCrossedHalf=1;
		pthread_mutex_unlock(&mutex);

		DBGsleep(1, "const char *stringMsg");

		pthread_mutex_lock(&mutex);
		printf("Orario %d finisce di passare biglietto %d\n",index,bigliettiOrari[index]);
		pthread_mutex_lock(&mutex_bigliettoOrario);
		if(numeroAutoOrario<=0){
			ponteStatus=0;
			DBGpthread_cond_broadcast(&cond_waitOrario, "const char *stringMsg"); /*sveglio gli antiorario*/
		}else{
			DBGpthread_cond_broadcast(&cond_waitAntiOrario, "const char *stringMsg"); /*sveglio gli orari*/
		}
		pthread_mutex_unlock(&mutex_bigliettoOrario);
		pthread_mutex_unlock(&mutex);
		DBGsleep(10, "const char *stringMsg");
	}

	pthread_exit(NULL);
}

void * antiorario(void * args) {
	int index = *(int *) args;

	while(1){
		/*biglietto*/
		pthread_mutex_lock(&mutex_bigliettoAntiorario);
		bigliettiAntiOrari[index]=bigliettoGlobaleAntiOrario;
		bigliettoGlobaleAntiOrario++;
		numeroAutoAntiOrario++;
		printf("AntiOrario %d prende biglietto %d\n",index,bigliettiAntiOrari[index]);
		pthread_mutex_unlock(&mutex_bigliettoAntiorario);
		
		pthread_mutex_lock(&mutex);
		while(verificaCondizioniAntiOrario(bigliettiAntiOrari[index])){ /*condizioni per cui non posso passare */
			DBGpthread_cond_wait(&cond_waitOrario, &mutex, "const char *stringMsg");
		}
		/*sono sciuro possa passare*/
		ponteStatus=2; /*sta passando un orario*/
		hasCrossedHalf=0;
		pthread_mutex_lock(&mutex_bigliettoAntiorario);
		numeroAutoAntiOrario--;
		pthread_mutex_unlock(&mutex_bigliettoAntiorario);
		printf("AntiOrario %d con biglietto %d inizia a passare sul ponte\n",index, bigliettiAntiOrari[index]);
		pthread_mutex_unlock(&mutex);
		
		DBGsleep(1, "const char *stringMsg");

		pthread_mutex_lock(&mutex);
		hasCrossedHalf=1;
		pthread_mutex_unlock(&mutex);

		DBGsleep(1, "const char *stringMsg");

		pthread_mutex_lock(&mutex);
		printf("AntiOrario %d finisce di passare biglietto %d\n",index,bigliettiAntiOrari[index]);
		pthread_mutex_lock(&mutex_bigliettoAntiorario);
		if(numeroAutoAntiOrario<=0){
			ponteStatus=0;
			DBGpthread_cond_broadcast(&cond_waitAntiOrario, "const char *stringMsg"); /*sveglio gli orario*/
		}else{
			DBGpthread_cond_broadcast(&cond_waitOrario, "const char *stringMsg"); /*sveglio gli antiorari*/
		}
		pthread_mutex_unlock(&mutex_bigliettoAntiorario);
		pthread_mutex_unlock(&mutex);
		DBGsleep(10, "const char *stringMsg");
	}

	pthread_exit(NULL);
}

int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_mutex_init(&mutex_bigliettoAntiorario, NULL, "Error");
	DBGpthread_mutex_init(&mutex_bigliettoOrario, NULL, "Error");
	DBGpthread_cond_init(&cond_waitOrario, NULL, "Error");
	DBGpthread_cond_init(&cond_waitAntiOrario, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 4; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i;
		 
		if (pthread_create( &th,NULL,orario,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	for(i = 0; i < 4; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i;
		 
		if (pthread_create( &th,NULL,antiorario,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	pthread_exit(NULL);
}
