#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond_waitIppo;
pthread_cond_t cond_waitLem;
pthread_cond_t cond_waitForTheEndOfTheRide;

int is_ippoInWater=0;
int can_lemuri_jump_in=0;
int numberoLemuriIN=0;

void * lemure(void * args) {
	int index = *(int *) args;
	while(1){
		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		while(can_lemuri_jump_in==0 || numberoLemuriIN>=4){
			DBGpthread_cond_wait(&cond_waitLem, &mutex, "const char *stringMsg");
		}
		/*può salire e non ci sono tanti lemuri*/
		printf("Lemure %d sale sull'ippopotamo\n", index);
		numberoLemuriIN++;
		/*aspetta di che l'ippo faccia tutto il suo viaggio */
		DBGpthread_cond_wait(&cond_waitForTheEndOfTheRide, &mutex, "const char *stringMsg");
		printf("Lemure %d scende dall'ippo\n", index);
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");

		DBGsleep(3, "ush arriva la tromba d'aria");
	}
	pthread_exit(NULL);
}

void * ippo(void * args) {
	int index = *(int *) args;
	while(1){
		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		
		while(is_ippoInWater){
			DBGpthread_cond_wait(&cond_waitIppo, &mutex, "wait for other ippo to finish");
		}
		/*nessun ippo è in acqua, quindi mi metto all'opera*/
		printf("Ippo %d si mette a dispoizione per accogliere lemuri\n", index);
		can_lemuri_jump_in=1;
		numberoLemuriIN=0;
		is_ippoInWater=1;
		DBGpthread_cond_broadcast(&cond_waitLem, "const char *stringMsg");
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");
		
		DBGsleep(2, "const char *stringMsg");

		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		can_lemuri_jump_in=0;
		printf("Ippo %d si mette in viaggio\n",index);
		DBGsleep(3, "const char *stringMsg");
		printf("Ippo %d arriva\n",index);
		is_ippoInWater=0;
		DBGpthread_cond_broadcast(&cond_waitIppo, "const char *stringMsg");/*sveglio l'altro ippo così può partire*/
		DBGpthread_cond_broadcast(&cond_waitForTheEndOfTheRide, "const char *stringMsg"); /*sveglio i lemuri sulla mia groppa*/
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");

		DBGsleep(3, "ush arriva la tromba d'aria");
	}
	pthread_exit(NULL);
}


int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond_waitForTheEndOfTheRide, NULL, "Error");
	DBGpthread_cond_init(&cond_waitLem, NULL, "Error");
	DBGpthread_cond_init(&cond_waitIppo, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 7; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,lemure,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	for(i = 0; i < 2; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,ippo,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	pthread_exit(NULL);
}
