#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"



#define NALPHA 5
#define NBETA 4

typedef struct {
	int campoOccupied;
	int lastUsedBy; /*0 A, 1 B*/
	int totGiriA;
	int totGiriB;
	pthread_mutex_t mutex;
	pthread_cond_t cond_wait_A_finish;
	pthread_cond_t cond_wait_B_finish;
}buffer;

void alfa(buffer *b, int index) {
	int numGiriFatti=0;
	while(1){
		DBGpthread_mutex_lock(&b->mutex, "lock var globali");
		while(numGiriFatti > b->totGiriA || b->campoOccupied || b->lastUsedBy == 0){ /*se il campo è occupato da un A o B oppure ho già fatto il mio giro oppure l'ultimo ad averlo usato è una a; -->aspetto */
			DBGpthread_cond_wait(&b->cond_wait_B_finish, &b->mutex, "cond aspetto gli altri partano");
		}
		/*è il mio turno, occupo il campo*/
		b->campoOccupied=1;
		printf("ALFA %d inizia la sua corsa\n", index);

		DBGpthread_mutex_unlock(&b->mutex, "lock var globali");
		DBGsleep(1, "const char *stringMsg");
		DBGpthread_mutex_lock(&b->mutex, "lock var globali");
		
		b->lastUsedBy=0;
		b->totGiriA++;
		b->campoOccupied=0;
		numGiriFatti+=NALPHA;
		printf("ALFA %d finisce la sua corsa\n\tgiri fatti da lui: %d\n\tgiri totali A: %d\n", index, numGiriFatti, b->totGiriA);
		DBGpthread_cond_broadcast(&b->cond_wait_A_finish, "Risveglio i B");
		DBGpthread_mutex_unlock(&b->mutex, "lock var globali");
	}
}

void beta(buffer *b, int index) {
	int numGiriFatti=0;
	while(1){
		DBGpthread_mutex_lock(&b->mutex, "lock var globali");
		while(numGiriFatti>b->totGiriB || b->campoOccupied || b->lastUsedBy == 1){ /*se il campo è occupato da un A o B oppure ho già fatto il mio giro oppure l'ultimo ad averlo usato è una a; -->aspetto */
			DBGpthread_cond_wait(&b->cond_wait_A_finish, &b->mutex, "cond aspetto gli altri partano");
		}
		/*è il mio turno, occupo il campo*/
		b->campoOccupied=1;
		printf("BETA %d inizia la sua corsa\n", index);

		DBGpthread_mutex_unlock(&b->mutex, "lock var globali");
		DBGsleep(1, "const char *stringMsg");
		DBGpthread_mutex_lock(&b->mutex, "lock var globali");
		
		b->lastUsedBy=1;
		b->totGiriB++;
		b->campoOccupied=0;
		numGiriFatti+=NBETA;
		printf("BETA %d finisce la sua corsa\n\tgiri fatti da lui: %d\n\tgiri totali B: %d\n", index, numGiriFatti, b->totGiriB);
		DBGpthread_cond_broadcast(&b->cond_wait_B_finish, "Risveglio i A");
		DBGpthread_mutex_unlock(&b->mutex, "lock var globali");
	}
}


int main() {
	int shmfd;
	int shared_seg_size = sizeof(buffer);
	pid_t pid = 0;
	buffer *b;
	int i = 0;
	pthread_mutexattr_t mattr;
	pthread_condattr_t cattr;

	if(shm_unlink("/temp") < 0) {
		printf("Error while deleting the file\n");
		exit(1);
	}

	shmfd = shm_open("/temp", O_CREAT | O_EXCL | O_RDWR, S_IRWXU);
	if(shmfd < 0) {
		printf("Error while creating the shared memory file\n");
		exit(1);
	}

	if (ftruncate(shmfd,shared_seg_size) != 0) {
		printf("Error\n");
		exit(1);
	}

	b = (buffer *)mmap(NULL, sizeof(buffer),  PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);

	/*initiate the buffer variables*/
	b->campoOccupied = 0;
	b->lastUsedBy = 0; /*0 A, 1 B*/
	b->totGiriA = 0;
	b->totGiriB = 0;

	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	DBGpthread_mutex_init(&b->mutex, &mattr, "Error while initializing the mutex");

	pthread_condattr_init(&cattr);
	pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED);
	DBGpthread_cond_init(&b->cond_wait_A_finish, &cattr, "Error while initializing the cond");
	pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED);
	DBGpthread_cond_init(&b->cond_wait_B_finish, &cattr, "Error while initializing the cond");

	for (i = 0; i < NALPHA; ++i) {
		pid = fork();
		if(pid < 0) {
			printf("Error while forking the process\n");
			exit(1);
		} else if(pid == 0) {/*son process*/
			alfa(b, (i + 1));/*son function*/
			return 0; /*if the process must terminate after the son function execution*/
		}
	}


	for (i = 0; i < NBETA; ++i) {
		pid = fork();
		if(pid < 0) {
			printf("Error while forking the process\n");
			exit(1);
		} else if(pid == 0) {/*son process*/
			beta(b, (i + 1));/*son function*/
			return 0; /*if the process must terminate after the son function execution*/
		}
	}


	return 0;
}
