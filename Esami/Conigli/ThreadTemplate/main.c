#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond_waitOutTana;
pthread_cond_t cond_waitAltroConiglio;
int conigliInTana=0;
int both_Exited=1;

int indiceGlobale=5; 
void * coniglio(void * args) {
	pthread_t th;
	int index = *(int *) args;
	int *intptr;
	DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
	while(conigliInTana>=2 || both_Exited==0){
		DBGpthread_cond_wait(&cond_waitOutTana, &mutex, "const char *stringMsg");
	}
	/*posso entrare */
	conigliInTana++;
	printf("Coniglio %d entra in tana \n", index);
	if(conigliInTana==1){
		DBGpthread_cond_wait(&cond_waitAltroConiglio, &mutex, "const char *stringMsg");
		DBGpthread_cond_broadcast(&cond_waitOutTana, "const char *stringMsg");
		both_Exited=1;
	}else{
		DBGpthread_cond_broadcast(&cond_waitAltroConiglio, "const char *stringMsg");
		printf("ACCOPPIANDOSI\n");
		both_Exited=0;
	}
	conigliInTana--;
	if(!(intptr = malloc(sizeof(int)))) { 
		printf("malloc failed\n");
		exit(1); 
	}
	*intptr = indiceGlobale + 1;
	indiceGlobale++;
	if (pthread_create( &th,NULL,coniglio,(void*)intptr)) {
		PrintERROR_andExit(1, "Error");
	}
	printf("Coniglio %d esce dalla tane\n",index);
	DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");
	free(intptr);
	pthread_exit(NULL);
}


int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond_waitAltroConiglio, NULL, "Error");
	DBGpthread_cond_init(&cond_waitOutTana, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 5; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,coniglio,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}


	pthread_exit(NULL);
}
