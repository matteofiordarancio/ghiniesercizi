#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond;

int currentCapacity=500;
int expectedCapacity=500;
int isPekariDrinking=0;

void * elefante(void * args) {
	int index = *(int *) args;
	while(1){
		pthread_mutex_lock(&mutex);
		while(expectedCapacity<100 || isPekariDrinking==1){
			DBGpthread_cond_wait(&cond, &mutex, "const char *stringMsg");
		}
		/*beve*/
		printf("Elefante %d beve dalla pozza\n",index);
		expectedCapacity-=100;
		pthread_mutex_unlock(&mutex);
		DBGnanosleep(100000000, "const char *stringMsg");
		pthread_mutex_lock(&mutex);
		currentCapacity-=100;
		if(currentCapacity<expectedCapacity){
			expectedCapacity=currentCapacity;
		}
		printf("Elefante %d finisce di bere.\n", index);
		pthread_mutex_unlock(&mutex);
		DBGsleep(1, "const char *stringMsg");
	}
	pthread_exit(NULL);
}


void * pekaro(void * args) {
	int index = *(int *) args;
	while(1){
		pthread_mutex_lock(&mutex);
		while(expectedCapacity<1){
			DBGpthread_cond_wait(&cond, &mutex, "const char *stringMsg");
		}
		/*beve*/
		isPekariDrinking=1;
		printf("Pekaro %d beve dalla pozza\n",index);
		expectedCapacity-=1;
		pthread_mutex_unlock(&mutex);
		
		DBGnanosleep(10000000, "const char *stringMsg");

		pthread_mutex_lock(&mutex);
		currentCapacity-=1;
		if(currentCapacity<expectedCapacity){
			expectedCapacity=currentCapacity;
		}
		printf("Pekaro %d finisce di bere.\n", index);
		isPekariDrinking=0;
		DBGpthread_cond_broadcast(&cond,"a");
		pthread_mutex_unlock(&mutex);
		DBGsleep(2, "const char *stringMsg");
		DBGnanosleep(200000000, "const char *stringMsg");
	}
	pthread_exit(NULL);
}


void * acquedotto(void * args) {
	while(1){
		pthread_mutex_lock(&mutex);
		if(currentCapacity<=430){
			currentCapacity+=70;
			expectedCapacity+=70;
		}else{
			currentCapacity=500;
			expectedCapacity=500;
		}
		DBGpthread_cond_broadcast(&cond, "const char *stringMsg");
		printf("Pozzo acqua %d\n", currentCapacity);
		pthread_mutex_unlock(&mutex);
		sleep(1);
	}
}

int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 10; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,pekaro,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

		/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 5; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,elefante,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	for(i = 0; i < 1; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i + 1;
		 
		if (pthread_create( &th,NULL,acquedotto,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}
	pthread_exit(NULL);
}
