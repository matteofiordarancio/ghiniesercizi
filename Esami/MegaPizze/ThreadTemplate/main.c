#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


pthread_mutex_t mutex;
pthread_cond_t cond_pizzaiolo;
pthread_cond_t cond_waitEndOfEating;
pthread_cond_t cond_waitTavolo;

int pizza_ready=0;
int prepare_pizza=0;
int persone_sedute_al_tavolo=0;
int are_eating=1;


void * pizzaiolo(void * args) {
	while(1){
		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		while(prepare_pizza==0){
			DBGpthread_cond_wait(&cond_pizzaiolo, &mutex, "const char *stringMsg");
		}
		/*preparo la pizza*/
		pizza_ready=1;
		printf("Pizzaiolo porta la pizza\n");
		prepare_pizza=0;
		DBGpthread_cond_broadcast(&cond_waitTavolo, "const char *stringMsg");
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");
	}
	pthread_exit(NULL);
}

void * persona(void * args) {
	int index = *(int *) args;
	while(1){
		/*arrivo, controllo che mi posssa sedere*/
		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		while(are_eating==0){
			pthread_cond_wait(&cond_waitEndOfEating, &mutex);
		}
		/*non stanno mangiando, quindi mi posso sedere */
		printf("Persona %d si siede per mangiare\n", index);
		persone_sedute_al_tavolo++;
		if(persone_sedute_al_tavolo==3){
			/*chiamo il pizzaiolo*/
			prepare_pizza=1;
			printf("Persona %d chiama pizzaiolo\n",index);
			pthread_cond_broadcast(&cond_pizzaiolo);
		}
		if(persone_sedute_al_tavolo==4){
			DBGpthread_cond_broadcast(&cond_waitTavolo, "const char *stringMsg");
			are_eating=0;
		}
		while(persone_sedute_al_tavolo!=4 || pizza_ready!=1){ /*non ci sono le persone o la pizza non è pronta*/
			DBGpthread_cond_wait(&cond_waitTavolo, &mutex, "const char *stringMsg");
		}
		printf("Persona %d mangia la megapizza\n",index);
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");
		
		DBGsleep(3, "");

		DBGpthread_mutex_lock(&mutex, "const char *stringMsg");
		persone_sedute_al_tavolo--;
		if(persone_sedute_al_tavolo==0){
			are_eating=1;
			DBGpthread_cond_broadcast(&cond_waitEndOfEating, "const char *stringMsg");
		}
		printf("Persona %d finisce la megapizza\n",index);
		DBGpthread_mutex_unlock(&mutex, "const char *stringMsg");

		DBGsleep(2+index, "const char *stringMsg");
	}
	pthread_exit(NULL);
}


int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex, NULL, "Error");
	DBGpthread_cond_init(&cond_waitTavolo, NULL, "Error");
	DBGpthread_cond_init(&cond_pizzaiolo, NULL, "Error");
	DBGpthread_cond_init(&cond_waitEndOfEating, NULL, "Error");

	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 5; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i;
		 
		if (pthread_create( &th,NULL,persona,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}

	if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
	}
	*intptr = i + 1;
	 
	if (pthread_create( &th,NULL,pizzaiolo,(void*)intptr)) {
		PrintERROR_andExit(1, "Error");
	}


	pthread_exit(NULL);
}
