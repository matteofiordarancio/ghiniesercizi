#!/bin/bash
if(($# != 2)); then
	echo "numero argomenti errato" 1>&2
	exit 1
fi

if [[ ! -e $1 ]] ; then
	echo "argomento non file" 1>&2
	exit 1
fi

if [[ ! -e output.txt ]]; then
	touch output.txt
fi

./esegui.sh "$1" "$2" &

exit 0

