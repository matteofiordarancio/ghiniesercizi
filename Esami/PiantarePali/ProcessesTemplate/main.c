#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


typedef struct {
	int startMartellare0;
	int startMartellare1;	
	pthread_mutex_t mutexPalo0;
	pthread_mutex_t mutexPalo1;
	pthread_cond_t cond_wait_TieniPali;
	pthread_cond_t cond_wait_Martelli;
}buffer;

void Martello0(buffer *b, int index) {
	int contaPali=0;
	while(1){
		DBGpthread_mutex_lock(&b->mutexPalo0, "lock palo 0");
		while(b->startMartellare0 != 1){
			DBGpthread_cond_wait(&b->cond_wait_TieniPali, &b->mutexPalo0, "wait Tieni pali");
		}
		printf("Martello0 inizia\n");
		DBGsleep(1, "const char *stringMsg");
		printf("Martello0 finisce %d\n",contaPali);
		contaPali++;
		b->startMartellare0 = 0;
		DBGpthread_cond_broadcast(&b->cond_wait_Martelli, "const char *stringMsg");
		DBGpthread_mutex_unlock(&b->mutexPalo0, "lock palo 0");
		if(contaPali>=4){
			printf("Martello0 si risposa %d\n",contaPali);
			DBGsleep(5, "const char *stringMsg");
			contaPali=0;
		}
	}
}

void Martello1(buffer *b, int index) {
	int contaPali=0;
	while(1){
		DBGpthread_mutex_lock(&b->mutexPalo1, "lock palo 0");
		while(b->startMartellare1 != 1){
			DBGpthread_cond_wait(&b->cond_wait_TieniPali, &b->mutexPalo1, "wait Tieni pali");
		}
		printf("Martello1 inizia\n");
		DBGsleep(1, "const char *stringMsg");
		printf("Martello1 finisce %d\n",contaPali);
		contaPali++;
		b->startMartellare1 = 0;
		DBGpthread_cond_broadcast(&b->cond_wait_Martelli, "const char *stringMsg");
		DBGpthread_mutex_unlock(&b->mutexPalo1, "lock palo 0");
		if(contaPali>=4){
			printf("Martello1 si risposa %d\n",contaPali);
			DBGsleep(5, "const char *stringMsg");
			contaPali=0;
		}
	}
}

void TienePalo(buffer *b) {
	int contaPali;
	while(1){
		DBGpthread_mutex_lock(&b->mutexPalo0, "lock palo 0");
		DBGpthread_mutex_lock(&b->mutexPalo1, "lock palo 1");
		printf("Presi pali\n");
		DBGsleep(1, "const char *stringMsg");
		printf("Martellare\n");
		b->startMartellare0 = 1;
		b->startMartellare1 = 1;
		DBGpthread_cond_broadcast(&b->cond_wait_TieniPali, "const char *stringMsg");
		while(b->startMartellare0 !=0 || b->startMartellare1 !=0){
			DBGpthread_mutex_unlock(&b->mutexPalo1, "lock palo 1");
			DBGpthread_cond_wait(&b->cond_wait_Martelli, &b->mutexPalo0, "wait marteli finiscano");
			DBGpthread_mutex_lock(&b->mutexPalo1, "lock palo 1");
		}
		DBGpthread_mutex_unlock(&b->mutexPalo0, "lock palo 0");
		DBGpthread_mutex_unlock(&b->mutexPalo1, "lock palo 1");
		if(contaPali<5){
			printf("Tiene Pali lascia i pali %d\n",contaPali);
			contaPali++;
		}else{
			printf("Tiene Pali si risposa %d\n",contaPali);
			DBGsleep(3, "const char *stringMsg");
			contaPali=0;
		}
	}
}


int main() {
	int shmfd;
	int shared_seg_size = sizeof(buffer);
	pid_t pid = 0;
	buffer *b;
	pthread_mutexattr_t mattr;
	pthread_condattr_t cattr;

	shmfd = shm_open("/temp", O_CREAT | O_EXCL | O_RDWR, S_IRWXU);
	if(shmfd < 0) {
		printf("Error while creating the shared memory file\n");
		exit(1);
	}

	if (ftruncate(shmfd,shared_seg_size) != 0) {
		printf("Error\n");
		exit(1);
	}

	b = (buffer *)mmap(NULL, sizeof(buffer),  PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);

	/*initiate the buffer variables*/
	b->startMartellare0 = 0;
	b->startMartellare1 = 0;

	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	DBGpthread_mutex_init(&b->mutexPalo0, &mattr, "Error while initializing the mutex");
	DBGpthread_mutex_init(&b->mutexPalo1, &mattr, "Error while initializing the mutex");

	pthread_condattr_init(&cattr);
	pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED);
	DBGpthread_cond_init(&b->cond_wait_Martelli, &cattr, "Error while initializing the cond");
	DBGpthread_cond_init(&b->cond_wait_TieniPali, &cattr, "Error while initializing the cond");
	

	pid = fork();
	if(pid < 0) {
		printf("Error while forking the process\n");
		exit(1);
	} else if(pid == 0) {/*son process*/
		Martello0(b, 0);/*son function*/
		return 0; /*if the process must terminate after the son function execution*/
	}

	pid = fork();
	if(pid < 0) {
		printf("Error while forking the process\n");
		exit(1);
	} else if(pid == 0) {/*son process*/
		Martello1(b, 1);/*son function*/
		return 0; /*if the process must terminate after the son function execution*/
	}

	/*father function*/
	TienePalo(b);
	if(shm_unlink("/temp") < 0) {
		printf("Error while deleting the file\n");
		exit(1);
	}

	return 0;
}
