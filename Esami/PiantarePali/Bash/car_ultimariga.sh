#!/bin/bash

for name in `find /usr/include/linux/  -maxdepth 1 -name "*.h" | grep "f"`; do
	out=`cat $name|wc -l`
	if(( $out>=10 && $out<=100)); then
		echo `tail -n 1 $name | wc -c` 
	fi
done
