#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"

int ready_A=0;
int ready_B=0;
int numeroVecchiCheGuardano=0;
pthread_mutex_t mutex_attacchini;
pthread_cond_t cond_wait_attacchini;
pthread_cond_t cond_wait_vecchi;


void * vecchi(void * args) {
	int index = *(int *) args;
	while(1){
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		while(ready_A==1 || ready_B==1){
			DBGpthread_cond_wait(&cond_wait_vecchi, &mutex_attacchini, "wait  finish work");
		}
		/*vecchi guardano/*/
		numeroVecchiCheGuardano++;
		printf("Vecchio di indice %d guarda il manifest\n",index);
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");

		DBGsleep(3, "const char *stringMsg");
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		numeroVecchiCheGuardano--;
		printf("Vecchio indice %d se ne va\n",index);
		if(numeroVecchiCheGuardano==0){
			printf("Non c'è più nessun vecchio che guarda\n");
		}
		DBGpthread_cond_broadcast(&cond_wait_attacchini, "const char *stringMsg");
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");
		DBGsleep(1+index*2, "const char *stringMsg");
	}
	pthread_exit(NULL);
}

void * attacchino1(void * args) {
	while(1){
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		ready_A=1;
		DBGpthread_cond_broadcast(&cond_wait_attacchini, "AO");
		while(ready_B!=1 || numeroVecchiCheGuardano>=1){
			DBGpthread_cond_wait(&cond_wait_attacchini, &mutex_attacchini, "const char *stringMsg");
			DBGpthread_cond_broadcast(&cond_wait_attacchini, "AO");
		}
		/*siamo entrambi pronti, quindi lavoriamo */
		printf("attacchino1 inizia lavorare\n");
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");
		
		DBGsleep(2, "const char *stringMsg");		
		
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		printf("attacchino1 va a prendere la colla\n");
		ready_A=0;
		DBGpthread_cond_broadcast(&cond_wait_vecchi, "const char *stringMsg");
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");
		
		DBGsleep(2, "const char *stringMsg");	
	}
	pthread_exit(NULL);
}

void * attacchino2(void * args) {
	while(1){
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		ready_B=1;
		DBGpthread_cond_broadcast(&cond_wait_attacchini, "AO");
		while(ready_A!=1 || numeroVecchiCheGuardano>=1){
			DBGpthread_cond_wait(&cond_wait_attacchini, &mutex_attacchini, "const char *stringMsg");
			DBGpthread_cond_broadcast(&cond_wait_attacchini, "AO");
		}
		/*siamo entrambi pronti, quindi lavoriamo */
		printf("attacchino2 inizia lavorare\n");
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");
		
		DBGsleep(2, "const char *stringMsg");		
		
		DBGpthread_mutex_lock(&mutex_attacchini, "const char *stringMsg");
		printf("attacchino2 va a prendere la colla\n");
		ready_B=0;
		DBGpthread_cond_broadcast(&cond_wait_vecchi, "const char *stringMsg");
		DBGpthread_mutex_unlock(&mutex_attacchini, "const char *stringMsg");
		
		DBGsleep(2, "const char *stringMsg");	
	}
	pthread_exit(NULL);
}




int main() {
	int i = 0, *intptr;
	pthread_t th;
	
	DBGpthread_mutex_init(&mutex_attacchini, NULL, "Error");
	DBGpthread_cond_init(&cond_wait_attacchini, NULL, "Error");
	DBGpthread_cond_init(&cond_wait_vecchi, NULL, "Error");

	if (pthread_create( &th,NULL,attacchino1,NULL)) {
		PrintERROR_andExit(1, "Error");
	}
	if (pthread_create( &th,NULL,attacchino2,NULL)) {
		PrintERROR_andExit(1, "Error");
	}


	/*Initialize variables, mutex, condvar and create thread*/
	for(i = 0; i < 4; i++) {
		if(!(intptr = malloc(sizeof(int)))) { 
			printf("malloc failed\n");
			exit(1); 
		}
		*intptr = i;
		 
		if (pthread_create( &th,NULL,vecchi,(void*)intptr)) {
			PrintERROR_andExit(1, "Error");
		}
	}
	
	pthread_exit(NULL);
}
