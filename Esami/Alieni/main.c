/* file:  algoritmo_fornaio_per_coda_FIFO.c
	  i clienti accedono da soli al bancone del pane
	  nell'ordine con cui hanno ottenuto il biglietto.
*/ 

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif


#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>	/* uint64_t intptr_t */
#include <inttypes.h>	/* uint64_t  PRIiPTR */
#include <sys/time.h>	/* gettimeofday()    struct timeval */
#include <pthread.h> 

#include "printerror.h"
#include "DBGpthread.h"


/* variabili da proteggere */
int alieniFuori=5;
int alieniDentro=0;
int id_can_exit=-1;
int id_other=-1;

int inizio_id=0;
/* variabili per la sincronizzazione */
pthread_mutex_t  mutexCasa;
pthread_cond_t cond_wait_forLeave;
pthread_cond_t cond_canLeave;

void *alieno (void *arg) 
{ 
	pthread_t   th; 
	int *ptr;
	int i;
	int id = *((int *) arg);
	printf("AL %d creato\n", id);
	
	while(1) 
	{	
		DBGpthread_mutex_lock(&mutexCasa, "Lock casa");
		while(alieniDentro>=2){
			DBGpthread_cond_wait(&cond_wait_forLeave, &mutexCasa, "aspetto esca un alino");
		}
		/*entro in casa*/
		id_can_exit=id_other;
		id_other=id;
		alieniFuori--;
		alieniDentro++;
		printf("AL %d entra in casa e sveglia %d\n", id, id_can_exit);
		DBGpthread_cond_signal(&cond_canLeave, "Esce l'altro dentro");
		while(id_can_exit != id){
			DBGpthread_cond_wait(&cond_canLeave, &mutexCasa, "aspetto entri un altro");
		}
		printf("AL %d se ne va e muore\n",id);
		DBGsleep(3, "const char *stringMsg");
		alieniDentro--;
		DBGpthread_cond_broadcast(&cond_wait_forLeave, "Sveglio chi aspettava uscisse un alieno");
		/*se non ci sono abbasstanza alieni ne creo*/
		if(alieniFuori<3){
			for(i=0;i<2;i++) {
				ptr = malloc(sizeof(int));
				inizio_id++;
				alieniFuori++;
				*ptr = inizio_id;
				pthread_create( &th, NULL,alieno,(void*)ptr); 
			}
		}
		DBGpthread_mutex_unlock(&mutexCasa, "Lock casa");
		break;
	}
	
	pthread_exit(NULL); 
} 


int main (int argc, char* argv[] ) 
{ 
	pthread_t   th; 
	int *ptr;
	DBGpthread_mutex_init( &mutexCasa, NULL,"pthread_mutex_init failed");
	DBGpthread_cond_init( &cond_wait_forLeave, NULL,"pthread_cond_init failed");
	DBGpthread_cond_init( &cond_canLeave, NULL,"pthread_cond_init failed");

	
	/* lancio i clienti */
	for(inizio_id=0;inizio_id<5;inizio_id++) {
		ptr = malloc(sizeof(int));
		*ptr = inizio_id;
		pthread_create( &th, NULL,alieno,(void*)ptr); 
	}

	pthread_exit(NULL); 

	return(0); 
} 
  
  
  
